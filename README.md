# Thallia Tree
A developer-friendly Jekyll blog theme. It contains the regular blog posts, a separate tidbits and portfolio section, and a search bar. 

## Installation
1. Either use Linux or Window's Ubuntu subsystem. 
2. Install Jekyll [following these directions](https://jekyllrb.com/)
3. Clone this repo with git
  `git clone git@gitlab.com:jaynky/thallia-tree-4-realz.git`.
5. launch in terminal with `bundle exec jekyll serve`
6. Visit [ http://127.0.0.1:4000/]( http://127.0.0.1:4000/)

## Usage
There are three main sections that you will be using:

1. includes
2. layouts
3. posts and tidbits

### Includes
In the _includes folder the important code blocks are
- **head**  contains meta information about the site such as its description, title, and links to CSS stylesheets.
- **navbar** has all the elements for navigating the site.
- **Jstyle.css** this is the main CSS file. Having CSS in your _includes directory makes initial loading faster, but due to caching, all subsequent loading is slower.
 
### Layouts

The above includes are added to the layouts to build the page. If includes are little code stencils than layouts are the paper you draw it on (okay bad analogy but you get the point).
there are 9 layouts. 6 for each of the pages, 1 is a layout of the posts, 1 basic layout applied to every other layout, and 1 is for a thank you page for subscribers.

#### Page Layouts
Page layouts are all pretty much the same. They combine includes, list posts (either for the blog, tidbits or portfolio respectively). The only different one is the search page. It contains a script I copied that searches for posts.

#### Basic Layout
Though simple, the basic layout deserves a description. It contains what every single page needs:
 - header
 - navbar
 - -the {{content}}
 - footer
### Posts, Portfolio, and tidBits
Though different content they essentially function the same. Each one of these pages looks in its respective directory `{% for x in site.x %}` for posts and then displays them in the format you choose. 
Example from tiBits:
```html
<div  div  class="col-md-5 text-center">

<h3><a  href="{{ tidBits.url }}"  class="post-link">{{ tidBits.title }}</a></h3>

<img  class="col-md-12"  src="{{ tidBits.image }}"  alt="{{ tidBits.title }}">

<p  class="tidBits-desc">{{ tidBits.description }}</p>
```

## contact
If you have any questions contact me through discord at Jay#3130. If you want to email me (expect  a longer delay) my email is jaynkystudio@gmx.com 

