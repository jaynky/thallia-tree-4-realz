---
title: Setting up MySQL On Ubuntu Linux
author_profile: true
description:
comments: true

---

I've had the idea for this project for awhile, and while it's still under wraps, I thought I'd share some of the software components needed to get it up and going!

This project requires a database, somewhere I could store a bunch of data. I've never delved into the realm of database stuff before, so this has all been a huge learning experience.

I chose to go with the MySQL database from a few friend's recommendations. SQL is pretty common nowadays, you may have heard of it from cybersecurity podcasts or on the internet, with SQL servers being vulnerable to what are called "SQL Injections", which is how I knew about SQL.

SQL (pronounced see-quel, not squirrel, like I originally thought) is what's called a "Structural Query Language". So it's like a language, a server, and kinda like a REPL all in one, and it's really weird to think about it that way. SQL is a relational database, which means it relates what you store via tables, and must have pre-defined data types for what you store in the tables. You can read more [here](https://www.guru99.com/introduction-to-database-sql.html) about different kinds of databases.

This stuff has been super hard to research, a lot of the resources for MySQL (the version of SQL I'm using) only told me how to use it, not why I was doing certain things, and it all got really confusing, so I decided to just start by creating a MySQL server with MariaDB (the most common open source Linux SQL distribution), changing the root user password for the server, and adding another user.

I didn't know [MariaDB](https://github.com/MariaDB/server) was a thing until [Gector](http://gectorsbox.net/) told me about it. MariaDB is an open source re-implementation of MySQL as far as I know, since the original MySQL is run by Oracle, who apparently has a lot of legal battles and other silly things going on.

`sudo apt update`, then ou can install MariaDB with:

```
sudo apt install mariadb-server
```

There are two versions you can install, `mariadb-server` and `mariadb-client`. If you're setting up the main "server" where data is being stored, and it's not set up anywhere else, then install `mariadb-server`. Otherwise, if you're accessing another database that's created, I believe you would install `mariadb-client`. I haven't formally verified that, so don't quote me on that quite yet.
