---

layout: post
title: 4 x 4 x 4 LED Rainbow Cube
image: https://farm5.staticflickr.com/4841/31527905587_b25baf742a_z.jpg
description: I used this project to learn some circuitry basics, as well as work on my soldering skills.
author_profile: true

---

![4x4x4cube](https://farm5.staticflickr.com/4841/31527905587_b25baf742a_z.jpg) 

The 4 x 4 x 4 rainbow LED cube was a small project I used to help me understand the basics of multiplexing on the Arduino and for honing my soldering skills.

### Documentation

[4 x 4 x 4 Cube Assembly](https://thalliatree.net/projects/4x4x4-rainbow-led-cube-hardware/)

[4 x 4 x 4 Cube Final Product](https://thalliatree.net/4x4x4-rainbow-led-cube-final-product/)
