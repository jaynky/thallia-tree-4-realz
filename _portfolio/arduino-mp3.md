---
layout: post
title: Arduino MP3 System with DFPlayer
image: https://live.staticflickr.com/65535/48614796001_d9c93ebd6d_k.jpg
description: Summer 2019 mini-project as a birthday present. Fully functional MP3 player with Arduino!
---

![Arduino MP3 DFPlayer](https://live.staticflickr.com/65535/48614796001_d9c93ebd6d_k.jpg)
This past summer (August 2019) I spent the last few breaks working on the [Arduino MP3 with DFPlayer](https://thalliatree.net/arduino-dfplayer-mp3-module.html). You can see more detailed content in the post linked.

## Project Features

* Customizable music library with SD card
* Skip Forward/Back buttons
* Pause/Play buttons
* Volume buttons
* LCD Screen that displays the song number you're listening to

## Skills Acquired

* Working with audio and speakers
* Reading and changing arduino libraries

## Proposed Redesign Features

* A better power supply circuit
* Sturdier and more functional encasing
