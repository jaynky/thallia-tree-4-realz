---
layout: post
image: https://farm8.staticflickr.com/7909/31527903547_26ef3cdffc_z.jpg
title: Binary Watch
description: I challenged myself to design and create a project entirely from scratch before I left for college, so I chose to design a watch that displayed the time in binary.

---

![Binary-Watch](https://farm8.staticflickr.com/7909/31527903547_26ef3cdffc_z.jpg)

I challenged myself to design and create a project entirely from scratch before I left for college, so I chose to design a watch that displayed the time in binary.

**Project Documentation**:
- [74HC595 Shift Registers - The Inspiration](https://thalliatree.net/74hc595-texas-instruments-shift-registers/)
- [Project Announcement](http://thalliatree.net/projects/project-hype-binary-wristwatch/)
- [ATmega328P Microcontroller + 74HC595 Shift Register Setup and Test](http://thalliatree.net/atmega328p-microcontroller-74hc595-shift-register-setup-and-test-binary-watch/)
- [Code - part 1](http://thalliatree.net/code-part-1-binary-watch/)
- [Code - part 2](http://thalliatree.net/code-part-2-binary-watch/)
- [Prepping the ATmega328P: bootloader burn, fuse-byte programming, and hardware test](https://thalliatree.net/prepping-the-atmega328p-bootloader-burn-fuse-byte-programming-and-hardware-test-binary-watch/)
- [Mock Up PCB](http://thalliatree.net/mock-up-pcb-binary-watch/)
- [Code - part 3](http://thalliatree.net/code-part-3-binary-watch/)
- [ATmega328P 32.768kHz, sleep modes, and new schematic](http://thalliatree.net/atmega32p-32-768khz-sleep-modes-and-new-schematic-binary-watch/)
- [Final code, schematic, and PCB](http://thalliatree.net/final-code-schematic-and-pcb-binary-watch/)
- [PCB 95% Complete](http://thalliatree.net/pcb-95-complete-binary-watch/)
- [Binary Watch : Complete](http://thalliatree.net/binary-watch-complete/)
- [Video of Watch Running](https://youtu.be/BdNp0CsC454)
                                                               

