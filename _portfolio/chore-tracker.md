---

title: Parental Digital Assitant (PDA) Chore Tracking System
layout: post
image: https://live.staticflickr.com/65535/33825125998_47a2acbbc7_z.jpg
description: In my freshman spring semester of 2018, I designed and coded a digital chore tracker with my design team.

---

![Parental Digital Assistant](https://live.staticflickr.com/65535/33825125998_47a2acbbc7_z.jpg)

[Portfolium Project: https://portfolium.com/entry/freshman-2nd-semester-project-pda](https://portfolium.com/entry/freshman-2nd-semester-project-pda)

In my freshman spring semester of 2018, with my design team I designed and created this chore tracker for children.

Each year in the engineering courses there is a Makers Competition, where students are put into teams, are given an arduino, and are told to make a project by the end of the semester that has to do with the theme.

The theme for Spring 2018 was smart children and baby products, so I came up with the idea for my team, and we went with a smart chore tracker that helps children start to learn how to organize themselves as the experience more difficult schooling and more chores to complete.

Our product competed against many others in the competition, and we won the Best Functioning Product award out of these categories:
* Best Functioning Product
* Best Product Pitch
* Best Innovative Idea
* Most Return on Investment

I learned how to write complex programs in C++ by building it in segments and debugging them before putting them all together. I learned how to manage a team as the team leader, delegating tasks to my teammates and making sure they were completed on time. I focused on building the circuit and writing the program, while my other teammate built the encasing and helped to write many of the reports.

## Features
* Easy to read display
* Simple interface for children
* Fun leveling up system for incentivising chores

## Proposed Redesign Features

* I would not use an arduino, the main problem in all of the circuitry and programming was due to there being not enough memory.
* A different driver or programming library
* Improved touch control functions

## Links
* [Gitlab Repository](https://gitlab.com/thallia/engr152/blob/master/pda/touchscreen/touchscreen.ino)

## Blog Post Development
* [Interfacing Arduino, Adafruit 7” TFT Touchscreen, and RA8875 Screen Driver](https://thalliatree.net/arduino-and-ra8875/)
* [Project Hype: Chore Tracker (P.D.A)](https://thalliatree.net/project-hype-chore-tracker/)
