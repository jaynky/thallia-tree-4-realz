---
layout: post
title: Oscillating Air Engine
image: https://farm8.staticflickr.com/7904/46120806415_a11aa6729c_m.jpg
description: In my freshman fall semester of 2018, I designed and fabricated an oscillating air engine.
---

![Air-Engine](https://farm5.staticflickr.com/4875/46416194592_4bca043473_z.jpg)

[Portfolium Project: https://portfolium.com/entry/oscillating-air-engine-52](https://portfolium.com/entry/oscillating-air-engine-52)

In my freshman fall semester of 2018, I designed and fabricated an oscillating air engine. This is a machined engine that ran on air power. The challenge was to design it from scratch--the shape, mathematics, and more--following through and getting it to fully run for a competition.

I learned all about the exact creativity of machining, and the process of making many design choices that all affect how your product will come out in the end. I encountered some challenges with the machining side of my design, but with the help of an advisor I was easily able to fix it and get my engine running really well.

In the end, participating in my college's wobble off, the competition where all the engines are tested according to the category they were designed for, my engine beat the school speed record by 500RPM and won second place in the speed category.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ARJ9thAdrRk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Features of the engine:

* Offset pistons for constant force
* Big flywheel to carry momentum
* Triangular angle to reduce gravity and friction
* Dual piston, single acting

## Skills Acquired:

* Machining on a manual lathe and mill
* Foundational solid modeling
* Troubleshooting mechanical problems

## Proposed Redesign Features:

* Ball bearings in the valve plates
* Aluminum pistons
* Shorter pivot screws
