---
layout: post
title: Simon Says on Basys3 FPGA in Verilog
image: https://live.staticflickr.com/65535/49170078898_db1bda48f9_c.jpg
description: Sophomore fall semester project was implementing a Simon Says state machine onto the Basys3 FPGA.
---

![Simon Basys3 FPGA Game](https://live.staticflickr.com/65535/49170078898_db1bda48f9_c.jpg)

I spent a semester learning about digital logic design fundamentals, and week by week applying those in the lab while learning Verilog.

This project was very challenging, but very successful. It full runs a simon says game, from start to finish.

You can see all the source code [here on my Gitlab profile](https://gitlab.com/thallia/simon-says).

<iframe width="560" height="315" src="https://www.youtube.com/embed/Q7wbqPaH5FU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Features:

* Fully functional "Simon Says" game
* Two optional cheat modes - press one button to repeat sequence
* Flip a switch to see the next 4 upcoming button presses

## Skills Acquired:

* Fundamental knowledge of digital design, logic gates, and microprocessors
* Fundamentals of Verilog
* Programming in state machines
* Understanding of clocks and timing
