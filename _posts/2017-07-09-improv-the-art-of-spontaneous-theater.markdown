---
layout: post
author: thalliatree
comments: true
date: 2017-07-09 06:17:48+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2017/07/09/improv-the-art-of-spontaneous-theater/
slug: improv-the-art-of-spontaneous-theater
title: 'Improv: The Art of Spontaneous Theater'
wordpress_id: 384
categories:
- Improv
tags:
- competition
- crow
- jokebusters
- theater
---

Today I rode home from one of my last improv competitions, where I helped prepare my team to compete against three others in a battle of laughter and jokes. It was a three day competition, and each team performed 12 games. It was exhausting, hilarious, and wonderful bonding time with people I hadn't taken much time to get to know. The theme of the competition this year was superheroes, and my team was the **JokeBusters**. (I ain't afraid of no joke!)

My team took 3rd place (though the judges told us we were incredibly close to reaching 2nd place), and I won the award for the most skilled and supportive team member. It was a humbling and amazing experience that I will always remember.

Acting...acting is all about taking a story and putting yourself in the character's shoes, immersing yourself in their world and becoming that person. You have a script and blocking, perhaps even choreography and music.

What would happen if you had none of those tools, only the power of your own brain?

This, my friends, is improv. Improv is short for improvisational theater, the kind of acting with no script. You stand on the stage with nothing but your good looks and infinite intellect, and you act out a scene off the top of your head.

I have been fortunate to partake in four improvisational teams as of this date through my local theater group, over the course of four years; I have been coached by over 7 people who have been strong and inspirational people not only in theater, but in my personal life as well.

You may be thinking, "Acting, in front of people, without a script. Are you _kidding me? _Aren't there rules to follow and structure to be had?"

Ah, yes, my friend. Have no fear, I shall introduce you to the fantastic art of improvisational theater:

Starting us off is the foundation of all stories and scenes: CROW.
Umm, isn't that a bird?
You can be fairly certain I'm laughing at you right about now.
CROW is an acronym consistently used in improv to help us improvisers remember what we actually need to be doing instead of trying to be funny.

C stands for _Character_. In every scene you want to know who you are. If you don't know who you are, you won't have an objective that drives the scene (something your character wants, usually), and there won't be anything interesting for you to do. That would be boring. Interesting characters are at the heart of improv, whether it be the milkman that delivers milk to your house every day, your reformed pirate friend who lost both legs in an intense sci-fi ship battle, or your mom.

R stands for _Relationship_. When you're improvising with someone, your character should have someone to hang out with, share some intense memories with about cheese festivals....perhaps they're long lost brothers, highschool sweethearts, best pineapple picking buddies, whoever they are, you need to know them. If you have a relationship, you have history with the person. If you have history with the person, that can influence the way your character interacts with them. (Of course this is all on the spot, so you have a literal split second to decide who you are and how you're related to the other person you're improvising with).

O stands for _Objective_. The objective is something that drives the scene, it can sprout from a relationship, where a drunk dude may be wanting to show off to his bestie that he can hit the bullseye of the target with a dart, while squatting, to prove that he is better than his friend (yes, that is a real example of a character I played. Earlier today, in fact). The objective is to find something that your character may naturally want. While trying to get what you want, it can create conflict with the other characters if they have opposing objectives.

Finally, W stands for _Where_. In improv, we have no props. No sets. Anywhere we are must be pantomimed, as if we were actually there in real life. Whether it be the Himalayas, a Civil War Re-Enactment, cooking in your kitchen, or a port-o-potty, you must create your surroundings by interacting with them.

Next up, I'll introduce the simplest rule in improv, the "Yes, And."

Yes, And is a term we use quite a bit. When your scene partner offers you something in a scene (offer means give you ideas to work with), you accept the offer and add onto it, such as:

**Scene Parter 1:** Ah, Joe. I've missed you since you left for the Amazon, do tell me about your work wrangling the gators.

**Scene Partner 2**: Why, Bill I am flabbergasted you even remember me. I've been gone for twenty-five years! I'm surprised you even recognized my with all these scars. Good to see you, ol' buddy.

If you see in that example there, SP2 _agreed_, and then gave SP1 more information about their relationship how long they've known each other, giving SP2 a chance to build even more off of that.
Now, lets see what would happen if we rejected the Yes, And concept.

**SP1**: Ah, Joe. I've missed you since you left for the Amazon, do tell me about your work wrangling the gators.

**SP2: **Excuse me? My name isn't Joe, it's _Steve._ And I wasn't in the Amazon wrangling alligators, I was at the pet store stroking bunny rabbits....for twenty-five years.

Here, SP2 completely rejected what SP1 was saying, and went with their own idea, which wasn't fun to watch, or very entertaining, for that matter. The Yes, And concept is a surprisingly tough one to begin using, because it makes you realize how much you want to say 'no' and implement your own ideas. While using your own ideas is good, if your partner offers you a line with good information in it, forget that idea about a buff twinkie dealer from the 1950s and agree with your parter and move on. It'll make the scene much smoother.

The third concept that I learned this year from my improv team is called, "You and me, here and now." This phrase has been nailed into my head all year by my coach.

You and me, here and now. What does that mean in improv terms? It means no one wants to watch a scene about two people talking about dogs, you want the scene focus in on the relationship between those two people, perhaps they are dog show rivals and have been fighting for the championship since they both came out of the womb.

Do you see how that can be a much more interesting thing than watching two characters converse about something other than themselves?

Next up: Don't try to be funny. Though improv is though of as comedy, you shouldn't try hard to get the laughs or the attention, improv is to make your scene partner look good and the laughs will come naturally. Sure, you can have a good pun or one-liner here and there, but it must fit in the context of the scene and you have to justify why you said what you did. Trying to be funny ends up making everyone look bad or awkward, and we don't really want that.

Improv is all about letting loose. One of the things I have learned the most in improv is to be confident in yourself and your choices. If someone asks you a question, such as:

**SP1**: How on earth did you find my long lost treasure chest of cheese?

You don't want to respond with a shrug and "Eh, I don't know." Huh? Of _course you know. _And in fact, it wasn't just a treasure map that led you there, but it was a treasure map on the back of a gum wrapper that you saved in your shoe from 2nd grade. You _do_ know how you tripped and fell and broke your arm off. When you're confident in your choices, your scene partner gains confidence as well, and you both think clearer and make better scene choices.

Lastly, my final bit of wisdom. Improv is _all about_ _failing._

Failing? Yes, failing.

Improvising a story is unexpected, random, and you never know what is going to come out of your mouth or your scene partner's mouth. If you fail, fail _big_. Make people _know_ that you failed. Because guess what? It's funny to the audience or anyone who is watching, and you can learn from your mistakes. Sometimes your fail makes the whole scene a new and improved story! Failing is at the heart and soul of improv, and if you ever join an improv class, it's something you will find yourself doing often.

It can be a hard thing to get used to, especially if you're someone who wants to get everything right the first time. But let me say this: the more you fail, the more you get comfortable with getting back up and trying again. Not only does this improve your improv (ha), but it also ends up applying to your real life.

Ever since I have taken part in the joys of improv, my life has gotten better because of it. I have learned to fail endlessly and get back up again, I have learned to be spontaneous and confident in my decisions. I have learned to lead a team and encourage the heck out of people, and gained an array of skills. It's amazing how useful thinking on your feet can be in real life when you're in a tight situation.

If you ever have the chance to take an improv class, take part in it, only if it's for one night. Not only will it improve your confidence and your abilities to speak in front of people, it'll change the way you live and accomplish things in your day-to-day life.

Not to mention, it's really fun to laugh.
