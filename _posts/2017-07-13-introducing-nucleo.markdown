---
layout: post
author: thalliatree
comments: true
date: 2017-07-13 04:34:16+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2017/07/13/introducing-nucleo/
slug: introducing-nucleo
title: 'Introducing: nucleo'
wordpress_id: 488
categories:
- projects
tags:
- laminar fountain
- learning
- nucleo
- project
- stm32
---

Don't you love getting exciting packages in the mail that are full of electronic cool stuff?

I know I do.

Today, thanks to [Arrow's](https://www.arrow.com/) free overnight shipping (with a purchase of $20 or more), I received my STM32F042K Nucleo. Check out this cutie:

[![](https://thalliatree.files.wordpress.com/2017/07/img_4629.jpg)](https://thalliatree.files.wordpress.com/2017/07/img_4629.jpg)
Isn't he adorable?

[![](https://thalliatree.files.wordpress.com/2017/07/img_4630.jpg)](https://thalliatree.files.wordpress.com/2017/07/img_4630.jpg)



My mentor, fouric, introduced me to the nucleo a while ago, but I haven't been able to buy one until now (yay for no money!).

I found a few projects by a cool dude named [fduignan](https://github.com/fduignan) on GitHub, who played around with doing some cool LED programming that's similar to a project I'm wanting to start soon. What better way to learn a device, a new programming language, and how to build things than to play with cute microcontrollers?

I've teamed up with one of my friends, he helped me co-captain our improv team, and we're going attempt building a laminar jumping water fountain. They look something like [this](https://youtu.be/SEqrRV0jMw8). We eventually want to include RGB LEDs into it, so it'll change colors, and then get those colors to change based on music that's playing. It'll be a challenge for sure, but we're going to learn lots, and that's the fun of it. I'm hoping to get the LEDs to work with the nucleo, if not, I'll use my raspberry pi.

When we start the process, I'll update here with pictures and the process. Hopefully I'll get my breadboard and RGB LEDs from Sparkfun soon so I can start playing with the code.

Until next time!
