---
layout: post
author: thalliatree
comments: true
date: 2017-07-25 02:00:25+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2017/07/25/lightshow-pi-early-tech-behind-the-laminar-fountain/
slug: lightshow-pi-early-tech-behind-the-laminar-fountain
title: LightShow Pi ~ Early Tech Behind the Laminar Fountain
wordpress_id: 621
categories:
- projects
tags:
- laminar fountain
- music
- project
- raspberry pi
---

Today was awesome. I made huge progress in the tech side of the laminar fountain project.

I found a software called [LightShow Pi](http://lightshowpi.org/), which was originally created for Christmas light shows, but many people have used it for smaller projects of their own.

The first step after getting an LED to flash with the Pi was to install the software. First I had to update all the packages 'n stuff on my system with `sudo apt-get update` and `sudo apt-get upgrade`. That took around thirty-minutes to fully install.

Next I cloned the LightShow Pi repository off of GitHub: `cd ~
git clone https://togiles@bitbucket.org/togiles/lightshowpi.git
`
And change to the stable branch of code: ```cd lightshowpi
git fetch && git checkout stable`

Once completed, you install the stuff! Just make sure you're in the "lightshowpi" directory. Then you can run: `sudo ./install.sh`.

This part tripped me up, because it took over two hours. Everything installed fine until it started running the line `Installing rpi-audio-levels...`. My Pi got really hot and stuck, so I rebooted and tried to reinstall, but it kept getting stuck. ![img_4697](https://thalliatree.files.wordpress.com/2017/07/img_4697.jpg)

I left it for about 2hrs (with many checks in between) before seeing that it had finished. Then I rebooted as the instructions said.

After that, the next step would be to build the actual LED setup on my breadboard. Initially I tried following [this link](https://docs.google.com/document/d/1x97JIu5xVInZMutTNeaHlnQuyoLHjf3h-ugIo64pGfI/edit), but only for a generic idea of how to wire the breadboard. I didn't have a breakout board, so I went back to basics and made a simple circuit like the one in my last post, but with eight LEDs instead of just one. [![](https://thalliatree.files.wordpress.com/2017/07/img_47001.jpg)](https://thalliatree.files.wordpress.com/2017/07/img_47001.jpg)Next I attached the pins of the Raspberry Pi (3.3V and Ground) to the breadboard:![img_4706](https://thalliatree.files.wordpress.com/2017/07/img_4706.jpg)

Next step was to figure out what GPIO pins the Pi was using for the project, the google doc said GPIOs 0-7. [I used this chart to locate them.](http://pi4j.com/pins/model-b-rev1.html)

![img_4708](https://thalliatree.files.wordpress.com/2017/07/img_4708.jpg)

![img_4712](https://thalliatree.files.wordpress.com/2017/07/img_4712.jpg)

Lovely mess, ain't it? The pins are attached to their respective LED, and each LED is also connected to ground.

Finally, I could plug it all in and test this thang! To test it and make sure you have all the right channels, you run `sudo python py/hardware_controller.py --state=flash`

Each LED blinked twice, and I knew I had the right setup.

To play music, there were a few more steps I had to take. First, to make sure I had audio output, I connected my audio jack to a portable speaker I had (that's the blue thing). Next I had to actually find the music. Using a Lexar USB, I pulled some mp3 files from my music collection and put them on the USB, which I connected to the Pi. Then using the command `sudo python py/synchronized_lights.py --file=/home/pi/Desktop/Music/singles/shelter.mp3`

With `--file=`, you continue the command with the file path of where your music is located.

After all that work, I got this wonderful result. [https://youtu.be/oHTUvcA52Bo](https://youtu.be/oHTUvcA52Bo)

Next step, to try it with RGB LEDs. Until next time!

{thallia}

P.S. I have a discord server where we discuss and solve problems like this! If you're having trouble setting up your config or working on a project that's similar, we'd love to see it! Come join us and create a digital hackerspace community :) [Join here!](https://discord.gg/sCkCkyw)
