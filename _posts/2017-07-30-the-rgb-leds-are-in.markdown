---
layout: post
author: thalliatree
comments: true
date: 2017-07-30 06:39:46+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2017/07/30/the-rgb-leds-are-in/
slug: the-rgb-leds-are-in
title: The RGB LEDs are IN
wordpress_id: 708
post_format:
- Quote
---

The second to last step of getting the tech for my laminar fountain is complete!

I ordered a pack of [100 RGB LEDs](https://www.amazon.com/gp/product/B01C19ENDM/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1), super bright (not to mention cheap), from Amazon, and they came yesterday. Very good quality materials. I tore apart my old wiring setup and got these new LEDs ready for action.

I stuck four of 'em into a breadboard and then calculated the resistance for each color using the voltage drop information from the sticker on the bag. The red LED had a drop of 2.0-2.2V, and the green/blue LEDs had a drop of 3.0-3.2V.

With this info, you can calculate the strength of resistor that you need. You take the input voltage of your device (in my case, 5V) and subtract the voltage drop from the initial voltage. 5V - 2.2V = 2.8V. Now I take the kind of current that runs through them, 20mA, and divide 2.8V by 20mA (or 0.02A), which results in 140 Ohms. I didn't have a 140 Ohm Resistor, but I did have a 150 Ohm, so I used that from my [handy resistor pack](//www.amazon.com/dp/B016NXK6QK/ref=cm_sw_r_cp_api_a-xFzbEF7HZ4V) I got from amazon for $8.

Doing the same calculations with the Green/Blue LEDs, I got 90 Ohms, which I used 100 Ohm resistors for.

With all the resistors hooked up and the jumper wires attached, this is what my setup looked like:

[![](https://thalliatree.files.wordpress.com/2017/07/img_4733.jpg)](https://thalliatree.files.wordpress.com/2017/07/img_4733.jpg)

It's basically a jungle of wires.

Here's the GPIO pins I hooked each one up to:

(RGB 1 - upper right) red=pin0, green=pin2, blue=pin 4
(RGB 2 - lower left) red=pin1, green=pin3, blue=pin
(RGB 3 - lower right) red=pin6, green=pin0, blue=pin
(RGB 4 - lower left) red=pin7, green=pin1, blue=pin3

The result was strikingly happy! Check it out:

[I Won't Let You Down - OKGO](https://youtu.be/M_qwLlry4Yc)

[My Type - Saint Motel](https://youtu.be/BRwoWGSZPjA)

I did have a bit of trouble with the sound portion of it all, somehow I configured the music to be ear-blastingly loud, and my little tiny iHome speaker didn't have any volume control. So after blowing up my ears multiple times trying to figure out what went weird, all I did was enter `alsamixer` into my terminal, and there were sound controls right there for me. Woohoo! Just a few up/down keystrokes and the sound was ready to go.

For the fountain, it's also very inconvenient that if I want any of this to work, I have to download the music I want to hear onto the raspi via flash drive. My friend and I decided that was dumb, so I researched how to get music by either bluetooth or some sort of airplay function. I discovered a fantastic program called [shairport-sync](https://github.com/mikebrady/shairport-sync), which works like any airplay device. It'll broadcast an airplay-like signal to your apple devices that allows your phone, ipad, or computer to access and live-stream media through.

Sadly, though, my raspberry pi is a bit too old to support the software. I was able to get it installed perfectly and my phone all connected, but when I attempted to play a song, the lights lit up once and nothing came out of the speaker. After figuring out that my hardware was the problem, my friend offered to pay for a new Raspberry Pi 3 that'll support the function much better. That's coming soon, so I get to reinstall and figure this all out on a new Raspi! Until then, I've made good progress.

After we figure out the new raspi, I'll order some insulated wire and get that hooked up to each LED that'll sit inside the fountain to make pretty colored water.

Bye for now!
