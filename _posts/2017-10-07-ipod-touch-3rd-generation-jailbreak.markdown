---
layout: post
author: thalliatree
comments: true
date: 2017-10-07 06:21:28+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2017/10/07/ipod-touch-3rd-generation-jailbreak/
slug: ipod-touch-3rd-generation-jailbreak
title: iPod Touch 3rd Generation Jailbreak
wordpress_id: 1085
post_format:
- Quote
---

I was originally going to trade in my old iPod 3G, but none of the places I wanted to take it would give me good money for it (which is entirely understandable, it's a ridiculously old model and hardly works anymore). I was going to take it apart to see the guts and glory, until my friend suggested that I load Linux onto it!

Now, finding a resource to do so is really hard, and I haven't found one that works so far. But I have successfully jailbroken the iPod, which is super cool in and of itself.

Before this, I'd never heard of jailbreaking. It's a way of removing software restrictions that Apple places on the device, so you can do things you wouldn't usually be able to do on an un-jailbroken device. It gives you root access to the OS files and you can download things that you couldn't normally get on the App Store.

I followed [this tutorial](http://www.iphonehacks.com/2012/06/jailbreak-ios-5-1-1-ipod-touch-4g-ipod-touch-3g-redsn0w-untethered.html), but thought I'd go through it here as documentation as well.

First, you plug the iPod into your computer. If you have data on it that you want to save, make sure to back it up. I did this all on OSX (reasons below).

Here are all the disclaimers and warnings from that tutorial:



	
  * **This guide is meant for iPod touch users**. iPad users can check out [this guide](http://www.iphonehacks.com/2012/06/jailbreak-ios-5-1-1-new-ipad-3-ipad-2-ipad-1-users-redsn0w-untethered.html).

	
  * Redsn0w 0.9.12b2 supports untethered jailbreak.

	
  * Redsn0w 0.9.12b2 supports iPod touch 4G, iPod touch 3G running on iOS 5.1.1.

	
  * Please ensure your iPod touch is on iOS 5.1.1 (Settings –> General –> About –> Version should be 5.1.1) before proceeding.

	
  * Please note that [jailbreaking your iPod touch](http://www.iphonehacks.com/jailbreak_iphone) may void your warranty and hence proceed with [caution](http://www.iphonehacks.com/2010/04/apple-cautions-users-against-jailbreaking-iphone-os.html) and at your own risk.

	
  * Please do not forget to backup your iPod touch before you proceed.

	
  * Please ensure you are running latest version of iTunes.


You can download the zip file from http://www.iphonehacks.com/download-redsn0w.

I thought it was a super sketch website at first and was skeptical of whether or not to download it, but I decided otherwise. After unzipping it, double click on the application. It looks something like this:

![img_4887.jpg](https://thalliatree.files.wordpress.com/2017/10/img_4887-e1507357408744.jpg)

If the iPod is connected, you should see that redsnow recognizes it at the bottom. Click on "jailbreak and install cydia", and the process should complete in little time!

Make sure to wait for the iPod to reboot before unplugging it. Then search for Cydia to appear in the apps (you can use search to find it), then wait a little longer for a second reboot before unplugging and using. You should be all set!

Later! o7
