---
layout: post
author: thalliatree
comments: true
date: 2017-10-16 01:56:24+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2017/10/16/not-soldiering-saw-der-ing/
slug: not-soldiering-saw-der-ing
title: not "soldiering", saw-der-ing
wordpress_id: 1110
post_format:
- Quote
---

Today was exciting. At my mentor fouric's college, they held intro and intermediate workshops on many different subjects for their students, including EagleCAD, KiCAD, Raspberry Pi, Arduino, and soldering!

I just got a soldering iron for myself and for an upcoming project I'm working on, but I've had no previous experience in the skill. So, lo and behold, I took the intro to soldering and the intermediate soldering workshop!

Here are my results:

![image5.jpeg](https://thalliatree.files.wordpress.com/2017/10/image5.jpeg)

This is an audio amplifier, where they taught us through-hole soldering.

![image4.jpeg](https://thalliatree.files.wordpress.com/2017/10/image4.jpeg)

When I finished, I tested it out and it worked perfectly!

In the afternoon, we made a tone thingy, which controlled the treble and bass.

![image1.jpeg](https://thalliatree.files.wordpress.com/2017/10/image1.jpeg)

This taught us about surface mount soldering. Surface-mount is a pain in the butt, but it was a good workshop to go through for the experience!

Now I understand more of the whole process, and I can buy some accessories for my soldering iron at home.

I was really hesitant to solder at first since I have asthma, but the fumes didn't bother me like I thought they would. Having a fan or ventilator at your station is a good idea though, so you're not inhaling the smoke.

Next time, I'll preview the new project I'm going to work on and all the parts!

Until then...
