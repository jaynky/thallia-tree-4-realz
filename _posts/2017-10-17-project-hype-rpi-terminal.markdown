---
layout: post
author: thalliatree
comments: true
date: 2017-10-17 01:00:46+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2017/10/17/project-hype-rpi-terminal/
slug: project-hype-rpi-terminal
title: 'Project Hype: RPi Terminal'
wordpress_id: 1135
categories:
- projects
post_format:
- Quote
tags:
- project
- raspberry pi
---

Over the past week or so, I've been ordering and collecting parts for two projects I've been itching to build since I went to my local [barcamp](https://en.wikipedia.org/wiki/BarCamp). And now that I've learned to solder, I can dive in head first.

The first project I'm going to complete is a [Raspberry Pi powered Linux laptop](https://makezine.com/projects/build-raspberry-pi-powered-linux-laptop-that-fits-your-pocket/?). This is a tiny, nintendo DS sized laptop that mainly runs terminal commands and code, but you can run video and audio on it and it'll work just as well!

Here are the supplies, and the supply list: [![](https://thalliatree.files.wordpress.com/2017/10/img_4918.jpg)](https://thalliatree.files.wordpress.com/2017/10/img_4918.jpg)



	
  * [Raspberry Pi 3 Model B](https://www.arrow.com/en/products/raspberrypi3/raspberry-pi-foundation) (I decided to go with something a little newer than the Pi2)

	
  * MicroSD card (you'll need 8GB or more) for the Pi

	
  * [Adafruit PiTFT 3.5" touchscreen 480x320](https://www.adafruit.com/product/2097) (this was actually supposed to go to the original Pi models, but I don't see why it wouldn't work with the Pi3. It's bigger, too!)

	
  * [Lithium-Ion Poly battery](https://www.arrow.com/en/products/258/adafruit-industries) (I chose 1200mAh for larger capacity)

	
  * [Wireless keyboard](https://www.amazon.com/Bluetooth-Keyboard-Wireless-Playstation-Smartphones/dp/B00H88HC4Y/ref=sr_1_14?)

	
  * [5.5V to 3.3V step down converter](http://www.ebay.com/itm/3V-Out-5V-to-12V-In-AMS1117-3-0-Step-Down-Linear-Voltage-Regulator-Module-/391834247531?)

	
  * [Adafruit Powerboost 1000c](https://www.arrow.com/en/products/2465/adafruit-industries)

	
  * [Female USB connectors](https://www.sparkfun.com/products/9011)

	
  * Audio/Video 3.5mm jack

	
  * [Power switch (a little switch to turn on the power)](https://www.sparkfun.com/products/14330)

	
  * [Backlight switch for the keyboard](https://www.sparkfun.com/products/102)

	
  * [WiFi USB adapter](https://www.amazon.com/Edimax-EW-7811Un-150Mbps-Raspberry-Supports/dp/B003MTTJOY/)

	
  * Case


All of these came out to approximately ~$140, which my grandma generously helped me pay for. We bought from ebay, Amazon, Sparkfun, and Arrow. I had little to no knowledge about many of the things used in this project, so I'll go over each item before writing out my to-do list.

The Raspberry Pi and I have had plenty of previous experience with each other, it's the little computer powering the whole thing.
The Adafruit PiTFT is a little touchscreen perfect for the size of computer I'm building.
The Lithium Ion Polymer battery was a confusing buy, since I'd never heard of the term mAh before. mAh stands for milliamp hours, which measures the amount of coloumbs (electrical charge) stored inside of it. (if you do the math like I had to, it helps make sense.)
The wireless keyboard I got off of Amazon for about $10, it has bluetooth capability, but says it only works with certain operating systems. We'll see about that.
The 5.5V to 3.3V step down converter was probably the hardest thing I had to find, but I got a good deal on [eBay](http://www.ebay.com/itm/3V-Out-5V-to-12V-In-AMS1117-3-0-Step-Down-Linear-Voltage-Regulator-Module-/391834247531?) for one thanks to my friend Gector.
The Adafruit Powerboost 1000c is I think what helps recharge the battery that powers the RPi and the keyboard.It's super tiny, and actually really cute.

I got most of the other stuff off of Sparkfun, two little 3-pin switches, the female USB connectors, the audio/video jack, and the wifi dongle.

As for the case, in the tutorial this guy uses cheap HDD cases. I could not find any that fit to my liking, they were all either the wrong size or shape, or they came from China and I was too impatient to purchase one from there. That's when I was struck by a brilliant idea. Why not 3D print the case? (I know, great idea.)

I attempted to teach myself Sketchup, but quickly realized that I was too inexperienced to try and model something myself. Gector offered to help, and he made an _awesome_ case for me that I'm getting 3D printed by one of my other friends.

Here's a preview of the case before I print it!![rpilaptopcase](https://thalliatree.files.wordpress.com/2017/10/rpilaptopcase.png)Once I get it all 3D printed I'll post more pictures with the dimensions.

According to the tutorial, my next steps are to desolder all the components off of the RPi, Adafruit PiTFT, and wire everything up to the keyboard and battery. I gotta solder new wires onto the USB, Audio/Video, and HDMI ports, and get the MicroSD of the RPi set up.

I definitely have my work cut out for me, but once this project is physically done, I want to write an OS (operating system) for the Raspberry Pi specifically to accompany this project.

I'll update soon as I start working on this!


