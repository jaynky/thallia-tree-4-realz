---
layout: post
author: thalliatree
comments: true
date: 2017-10-24 04:49:52+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2017/10/24/raspberry-pi-mini-laptop-update-1/
slug: raspberry-pi-mini-laptop-update-1
title: Raspberry Pi Mini Laptop - Update 1
wordpress_id: 1210
post_format:
- Quote
---

So the process has begun.....

I'm getting the case for my mini laptop printed very soon, my friend and I are making some final modifications before getting that done.

In other news, I broke my Adafruit PiTFT 3.5" accidentally, so I had to buy a new one, which should be here tomorrow. This one is specifically for the Pi 3, so hopefully all the trouble I had getting my previous one to work will be gone!

I've been experiencing an _extreme_ amount of trouble with getting Raspbian to configure for the PiTFT, and I'm not sure why. Following the Adafruit tutorial (easy install) with the pre-made boot image gets stuck in the first boot process, at a file called `rc-local.service`.

It seems that this version of Raspbian is possibly defective, according to [this Stack Exchange](https://raspberrypi.stackexchange.com/questions/54521/raspberry-pi-wont-boot-up-after-installing-adafruits-jessie-pitft-image) question, which means I'm back to the drawing board. I've attempted to install the latest version of Raspbian (as of now, Raspbian Stretch) and download the kernel on top of it (detailed install on adafruit tutorial), but that throws many different errors about the repository of code being unsigned and unauthorized, so I'm not able to access it.

I haven't had any luck with [booting from a flash drive](http://www.instructables.com/id/Boot-the-Raspberry-Pi-from-USB/) either, so I'm going to get a new SD card since the one I'm using has been finnicky since the beginning. If it's still acting up with a new one, I know something is definitely wrong with either the way I'm doing things or the actual software.

On the happy side of things, I came up with a name for this project. Behold, the ArkenPi!

...get it? (Like the Arkenstone from the Hobbit?) :)
