---
layout: post
author: thalliatree
comments: true
date: 2017-10-25 06:23:35+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2017/10/25/arkenpi-install-config/
slug: arkenpi-install-config
title: ArkenPi - install & config
wordpress_id: 1238
post_format:
- Quote
---

Today has been a historic day. I've never attempted SSH before, but I tried to SSH into my printer instead of my raspberry pi.

Then I wget-ed the HTML of a webpage instead of the actual file.

It's been quite the day.

I bought a new microSD card since my other one was being ridiculously finnicky, and things are working much much better! I decided to try writing to the SD card a different way, where I unzipped the .zip file onto the SD instead of onto my desktop, _then_ writing the image to the card.

Surprisingly, the OS that worked for me was the latest NOOBS distribution. When I attempted to write straight Raspbian Stretch, I got errors about some of the files in the .zip being damaged, so I installed Raspbian from NOOBS, which installed everything perfectly. The version I have is still stretch, but a seemingly healthier version.

Next, I installed Adafruit's Occidentalis, which is a collection of packages useful for the raspberry pi. I did that by running:

```
sudo -s
echo "deb http://apt.adafruit.com/raspbian wheezy main" >> /etc/apt/sources.list
```

Then I ran `curl -SLs https://apt.adafruit.com/install | sudo bash`, which installed a _bunch_ of packages. There was the occasional error, but it installed what was needed.

The raspberrypi-bootloader and raspberrypi-kernel were already installed, so all I had to do next was `sudo apt-get install adafruit-pitft-helper`. I don't understand why, but authentication and unverified repositories came up. I installed the packages anyway.

There is an error after you type 'y', which is `error: /etc/kbd/config: no such file or directory`. All you do to fix it is `cd` into `/etc` and `mkdir kbd`, then `sudo nano /etc/kbd/config` and save the file with `Ctrl+O`. Then head back to your home directory and it should work. :)

And yet, with all that done, the PiTFT still didn't work. I was at a loss.

Then I found this forum: https://forums.adafruit.com/viewtopic.php?f=50&t=91209&p=465452#p465452

And my life was saved. If you download the .txt file, there's a good set of instructions that take you step by step through how to set up the PiTFT.
I'll update when I figure out whether or not it works, but if you attempt that way of doing things, just know that the old repos:

```
wget https://github.com/notro/fbtft_tools/raw/master/rpi_power_switch/rpi_power_switch.c
wget https://github.com/notro/fbtft_tools/raw/master/rpi_power_switch/Makefile
```

are gone, and you have to have to git clone the whole adafruit repository here:
`https://github.com/adafruit/adafruit-raspberrypi-linux`

Once I get this all done, I'll put up a tutorial and show you what things should look like!

Until then...
