---
layout: post
author: thalliatree
comments: true
date: 2017-10-26 06:41:17+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2017/10/26/arkenpi-the-touchscreen/
slug: arkenpi-the-touchscreen
title: ArkenPi ~ the touchscreen (Adafruit PiTFT Plus 3.5”)
wordpress_id: 1299
categories:
- projects
tags:
- arkenpi
- project
- raspberry pi
---

After hours of errors, outdated repos, and problem solving, I have _finally_ gotten ArkenPi's screen working!

[![](https://thalliatree.files.wordpress.com/2017/10/img_4983.jpg)](https://thalliatree.files.wordpress.com/2017/10/img_4983.jpg)
Here's how I did it:

First, install the most recent form of Raspbian. Turns out, the kernel is already updated to work with the Adafruit PiTFT 3.5" PLUS, so you don't have to download it from the outdated, unauthorized apt.adafruit.com repositories (thank goodness).

Boot into your Raspbian OS and make sure you're connected to a network, either way of ethernet, wifi adapter, or wirelessly (if you have a Pi 3). Go ahead and change your root password with `passwd`, then get into the raspberry pi configuration station.

Enter `raspi-config` into your terminal, and here's what we're gonna do:
1) Expand the filesystem
2) Change the default Pi account password
3) Enable Boot to Desktop (already signed in)
4) Enable Wait for Network at Boot
5) If you so wish, change your timezone
6) Set your hostname (the name of your pi)
7) Enable SSH (very handy if you run into errors)

Then select `finish`, `reboot`, and you'll be set to go!

Once your Pi has rebooted, login as root `sudo -s`, and head back to `raspi-config`.
Head to `Update`, and update all the raspberry pi config settings. After that you're free to exit.

Next, in a terminal, run `apt-get update && apt-get -y upgrade`.
Then you're going to download some code from github.

Use `wget https://raw.githubusercontent.com/notro/rpi-source/master/rpi-source -O /usr/bin/rpi-source` to download it and put it somewhere.
We're going to use chmod to make it executable like this:
`chmod +x /usr/bin/rpi-source`
then `/usr/bin/rpi-source -q --tag-update`.

After that, the guide I linked to in the previous post told me to wget some more driver code, but that code was outdated and removed from the repository. I didn't really understand what the code was for, but I'm glad I looked.

The code was to initialize a GPIO pin as a power switch. That was a great idea and all, but I have another way of doing it hardware-wise which was much easier to figure out.

Next, we're going to `cd` back to the Home directory and install the adafruit helper:
`wget https://github.com/adafruit/Adafruit-PiTFT-Helper/raw/master/adafruit-pitft-helper`
`chmod +x ./adafruit-pitft-helper`
and to run it, `./adafruit-pitft-helper -t 35r`
Now, as shown above, the `-t` stands for `type`, and then you specify which PiTFT you have for your Raspberry Pi. Since mine was the 3.5" Resistive touchscreen, `35r` was my choice.

Finally, you can run `shutdown -h now`, set up the PiTFT on your RPi, and reboot to see it working!

Now, if you were like me, a few weird things happened when I started up. First, the Pi was still outputting the desktop onto my HDMI console, and only part of the boot process was actually showing up on the PiTFT.

In my config files, I made sure `/etc/X11/xorg.conf.d/99-fbdev.conf` had the line `/dev/fb1` in it (wherever it was), because `fb1` is the PiTFT output.

To fix this, I pressed `Ctrl+Alt+*all the Fx keys*`. Eventually one of the screens shifted to a login screen to my Pi, but logging in didn't work. I would enter the right password, the screen would turn black with an underscore in the top left corner, then it would take me back to the login screen.

Turns out there's a pesky little file you've got to delete:

`sudo rm /home/pi/.Xauthority`Once I deleted that and rebooted, my PiTFT successfully displayed my desktop screen. Woohoo!!

Many times if I goofed and couldn't access the desktop because I accidentally enabled boot-to-console, I would SSH into my raspberry pi. I must say I love SSH, I could do it all day. It's such a cool thing to do. :D

The only thing I'm struggling with now is actually getting the touchscreen calibrated the right way, which isn't working.
I'm taking a break from the software side of this project to get oriented with how I'm going to put it all together in my 3D printed case, which is almost done. We're getting to the exciting part of this now!
