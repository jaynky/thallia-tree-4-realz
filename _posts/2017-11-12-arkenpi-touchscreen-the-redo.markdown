---
layout: post
author: thalliatree
comments: true
date: 2017-11-12 20:47:47+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2017/11/12/arkenpi-touchscreen-the-redo/
slug: arkenpi-touchscreen-the-redo
title: ArkenpiTFT ...The Redo
wordpress_id: 1516
post_format:
- Quote
---

I was fiddling around with calibrating the PiTFT plus last night, and unfortunately, the Pi started booting to the HDMI instead of the TFT again. Needless to say, I was very grumpy.

Since I knew that the latest forms of Raspbian already supported the PiTFT and you don't have to install the adafruit kernel: I said, "Screw this, I'm starting all over again."

I wiped my SD card.

```
fdisk -l
fdisk /dev/sdx
d #delete partition
n #new partition
t #type
b #W95 FAT32
w #write
mkfs.vfat /dev/sdx1
```

I rewrote the latest Raspbian Stretch with the Desktop (2017-09-07).

`dd bs=512k if=/home/thallia/2017-09-07-raspbian-jessie.img of=/dev/sdx`

Once successfully booted into Raspbian Stretch on the HDMI output, I _only_ cloned the Adafruit PiTFT Helper.

```
sudo apt-get update
git clone https://github.com/adafruit/Adafruit-PiTFT-Helper.git
```

Next I made the adafruit helper executable.

```
cd Adafruit-PiTFT-Helper
chmod +x adafruit-pitft-helper2.sh
```

I ran the install.

`sudo ./adafruit-pitft-helper2.sh `

In the middle of the install, it opened the options to configure the Pi for the TFT. It listed the types of screens in numbers from 1-5, in which I chose #4 (35r).
I chose "y", and then magic! It rebooted onto the PiTFT.

It booted to the console, but changing the raspi-config settings to Desktop only booted the desktop to the HDMI.

I fixed this by following the answer on [this](https://www.raspberrypi.org/forums/viewtopic.php?p=485302) forum.

`sudo apt-get install xserver-xorg-video-fbdev`

Next I made a file with the directory /usr/share/X11/xorg.conf.d/99-fbdev.conf, (` sudo nano /usr/share/X11/xorg.conf.d/99-fdbev.conf` and pasted this stuff in there:
```
Section "Device"
Identifier "myfb"
Driver "fbdev"
Option "fbdev" "/dev/fb1"
EndSection
```

After a reboot, my Pi was working a _lot_ faster and it successfully booted the Desktop GUI onto the TFT.

I also changed my pi's desktop photo to Marvel, because it looks freaking awesome.
