---
layout: post
author: thalliatree
comments: true
date: 2017-11-27 04:33:14+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2017/11/27/arkenpi-3d-printed-case/
slug: arkenpi-3d-printed-case
title: Arkenpi - 3D Printed Case
wordpress_id: 1565
post_format:
- Quote
---

Aaaand here she is!

![img_5131](https://thalliatree.files.wordpress.com/2017/11/img_5131.jpg)

Left to right, top to bottom: Keyboard bevel, Touchscreen bevel, case base, case top

![img_5139](https://thalliatree.files.wordpress.com/2017/11/img_5139.jpg)

Front closed ^

![img_5136](https://thalliatree.files.wordpress.com/2017/11/img_5136.jpg)

Back closed ^
Final product:

![img_5135](https://thalliatree.files.wordpress.com/2017/11/img_5135.jpg)

Credit to [Gector](https://thalliatree.wordpress.com/about/) for making the original design on fusion 360, and Jarin Alvarez for finishing the final design and printing it for me!

You can find the STL files on [my github](https://github.com/thallia/arkenpi).

At the moment, the case is in my garage drying, I spray painted it! ![](https://thalliatree.files.wordpress.com/2017/11/img_5187.jpg)![img_5188.jpg](https://thalliatree.files.wordpress.com/2017/11/img_5188-e1511757333979.jpg)

The last thing I have to do once it's dry is assemble it all together. Almost to the finish line!
