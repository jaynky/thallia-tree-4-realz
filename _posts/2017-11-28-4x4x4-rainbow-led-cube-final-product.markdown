---
layout: post
author: thalliatree
comments: true
date: 2017-11-28 08:38:03+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2017/11/28/4x4x4-rainbow-led-cube-final-product/
slug: 4x4x4-rainbow-led-cube-final-product
title: 4x4x4 Rainbow LED Cube - Final product!
wordpress_id: 1591
post_format:
- Quote
---

After connecting all the pins from the board into my Uno, all I had to do was plug in the power.

I preloaded a program onto the Uno off of the tutorial: [http://www.makeuseof.com/tag/how-to-make-a-pulsating-arduino-led-cube-that-looks-like-it-came-from-the-future/](http://www.makeuseof.com/tag/how-to-make-a-pulsating-arduino-led-cube-that-looks-like-it-came-from-the-future/)

And this was the result!

[https://youtu.be/Uc7oN0g_zVk](https://youtu.be/Uc7oN0g_zVk)

I can adjust the speed if I so wish.
I'm hoping to see if I can hook this up to music and get it to dance to the sine waves like the laminar fountain, but we'll see.

{thallia}
