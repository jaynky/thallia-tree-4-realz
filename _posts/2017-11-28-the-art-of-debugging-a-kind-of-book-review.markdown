---
layout: post
author: thalliatree
comments: true
date: 2017-11-28 05:02:09+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2017/11/28/the-art-of-debugging-a-kind-of-book-review/
slug: the-art-of-debugging-a-kind-of-book-review
title: 'The Art of Debugging: A Kind of Book Review'
wordpress_id: 1585
categories:
- debugging
- Learning Techniques
---

Over the summer I read the book Debugging: The 9 Indispensable Rules For Finding the Most Elusive Software and Hardware Problems by David Agans.

The rules and processes he demonstrates in the book are thorough, solid, and extremely helpful when it comes to issues I've faced in my projects, programming, and understanding of how a computer works (even when I'm only reasoning through things in my head I've used these rules!).

These rules can not only be applied to computers, but attempting to figure any kind of problem in general.

**Rule 1: Understand the System**

Before you attempt troubleshooting, if you understand the system and how it's supposed to work, reading the manual and figuring out the way all the pieces connect will aid you in the long run when you run into trouble. It'll give you a good road map to follow if you get lost in bugs or issues.

**Rule 2: Making It Fail**

By far the best rule I've used. Stimulate the failure, don't simulate it. As David Agans says, "Know that 'that' can happen." By making something fail over and over again, in different ways, perhaps generating new errors, it gives you a better feel for what's going on in the system.

**Rule 3: Quit Thinking and LOOK.**

Don't keep hypothesizing, look at the internals about what's_ actually_ going on. If you can literally "see" what's going on, it'll help you narrow down the possibilities.

**Rule 4: Divide and Conquer**

Find the range at which the bug is occurring, find the patterns, and allow that to help you narrow the search. As David Agans puts it, "If there's goo, the pipe is upstream. If there's no goo, the pipe is downstream."

**Rule 5: Change ONE Thing At A Time**

Another one of my favorite rules. I used this tactic in building my mini computer when I had to figure out which signals were coming off of which GPIO pins to turn on the touchscreen. If you change one thing, it'll aid in ruling out what's _not_ causing the issue and help you find what _is._

**Rule 6: Keep An Audit Trail**

This one is incredibly important. Sometimes you can't stay up until 3am attempting to fix a bug, and when you wake up in the morning or come back after school, you forgot what you tried the previous day. Simply keeping a file in the project folder of your computer or writing things down on sticky notes can boost your productivity because you're able to remember the smaller details of the steps you took.

**Rule 7: Check The Plug**

If you got this far and still haven't figured it out, go back to the basics. Is it plugged in the right way? Did you make sure to recompile it before running? Start at the beginning and make sure all your assumptions are correct.

**Rule 8: Get A Fresh View**

Getting an outside view on an issue or error can be a lifesaver. Don't be too proud to ask for fresh insights or ask someone with more experience, they've all been where you have. "Bugs happen. Take pride in getting rid of them, not getting rid of them by yourself." ~David Agans
When doing so, provide symptoms of what's going on, don't give someone else your theory of why it's happening. Not only will you learn what's going on better, but it'll give better information to the person you're asking to help you.

**Rule 9: If You Didn't Fix It, It Ain't Fixed**

A bug never decides to flit away on its own. If it randomly disappears, something else is likely wrong. Make sure it's fixed by stimulating the failure, and if it isn't fixed, head back to the drawing board.

Don't forget to take brain breaks, debugging can be ugly and annoying.

I really enjoyed David Agan's book, there's _much _more content than anything I just shared. I highly recommend reading it, whether your a programmer, engineer, DIY-ist, even someone who just enjoys problem solving.

happy debugging!
