---
layout: post
author: thalliatree
comments: true
date: 2017-12-05 08:54:46+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2017/12/05/edimax-ew-7811un-wifi-adapter-on-freebsd-server/
slug: edimax-ew-7811un-wifi-adapter-on-freebsd-server
title: Edimax EW-7811Un Wifi Adapter on FreeBSD Server
wordpress_id: 1625
categories:
- projects
post_format:
- Quote
tags:
- networking
- server
---

Servers are _awesome_. They're like huge hunks of metal and machinery that can perform duties like the big supercomputers they are.

I acquired a massive, old, and clunky Dell server from one of the UTW dudes, of which I have now named Big Bertha. She's a feisty one, I tell ya.

Because I want to have her running 24/7 in my garage or closet, I had to get a WiFi adapter working so she could run without ethernet.

Through a post on the FreeBSD forums, I determined the driver that worked alongside the Edimax 7811Un adapter was `urtwn0`. I believe you can also run these in the terminal if you're still not sure, and it'll return a driver:
For FreeBSD 10 and under:
`ifconfig | grep -B3 -i wireless`

For FreeBSD 11 and higher:
`sysctl net.wlan.devices`

Once determined, it was a matter of putting some lines in config files.

In `/etc/wpa_supplicant.conf`, put in:

```
network={
              ssid="myssid"`
              psk="mypsk"`
}
```

Where _myssid_ is the name of your network, and _mypsk_ is the password.

In `/etc/rc.conf`, put:

```
wlans_urtwn0="YES"
ifconfig_wlan0="WPA SYNCDHCP"
```

Where `urtwn0` is the replaced by the driver that works with your server.

Finally, put the code below in `/boot/loader.conf`:

`if_urtwn_load="YES"`

And because I knew the edimax adapter used a Realtek chip, I added this to the same loader file:

`legal.realtek.license_ack=1`

After that, reboot your server OR run `service netif restart`.
If everything works correctly, you should be able to `ping google.com` or `ping 8.8.8.8` and get responses back.

{thallia}

P.S. I have a discord server where we discuss and solve problems like this! If you're having trouble setting up your config or want to share what your cool config looks like, we'd love to see it! Come join us and create a digital hackerspace community :) [Join here!](https://discord.gg/sCkCkyw)
