---
layout: post
author: thalliatree
comments: true
date: 2017-12-31 00:47:54+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2017/12/31/fish-shell-prompt-customization/
slug: fish-shell-prompt-customization
title: Fish Shell - Install and Prompt Customization
wordpress_id: 1651
post_format:
- Quote
---

Instead of the generic bash shell that comes with Ubuntu Linux, I installed the fish shell a while ago.

Fish is awesome because it guesses your tab completion, provides paths that you visit frequently, and is much more lightweight than bash.

[http://fishshell.com/](http://fishshell.com/)

```
sudo apt-get update
sudo apt-get install fish
```

To make fish your default shell:
```
chsh -s `which fish'
```
Then logging out and logging back in should put it into effect.

Because I don't like the generic bash/fish prompts, I changed the prompt myself in the `.config/fish/functions/fish_prompt.fish`.

Here is my config file: [https://github.com/thallia/dotconfig/tree/master/fish/functions/fish_prompt.fish](https://github.com/thallia/dotconfig/tree/master/fish/functions/fish_prompt.fish)

shiverwind:

![fish](https://thalliatree.files.wordpress.com/2017/12/fish.png)

rainbow:

![fish1](https://thalliatree.files.wordpress.com/2017/12/fish1.png)

fire:

![fish2](https://thalliatree.files.wordpress.com/2017/12/fish2.png)

{thallia}

P.S. I have a discord server where we discuss and solve problems like this! If you're having trouble setting up your config or want to share what your cool config looks like, we'd love to see it! Come join us and create a digital hackerspace community :) [Join here!](https://discord.gg/sCkCkyw)
