---
layout: post
author: thalliatree
comments: true
date: 2017-12-31 07:52:45+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2017/12/31/openbox-on-macbook-pro-ubuntu/
slug: openbox-on-macbook-pro-ubuntu
title: OpenBox on Macbook Pro + Ubuntu
wordpress_id: 1650
---

Openbox is a really cool window manager that is useful for us hackers who wish to configure _everything_ about how our desktop looks.

Openbox allows you to change keybindings really easily, configure the taskbar, how you manage windows, and much more that I haven't discovered yet.

To install, you just have to run `sudo apt-get install openbox obconf tint2`

After that, restart your laptop, then in the little gear icon, click on the OpenBox option.

Due to my HiDPI macbook screen, the scaling was way off. We were able to fix that with:

`xrandr --output eDP-1 --scale 0.6x0.6`

eDP-1 was the output of `arandr`, a window size editor.

When I converted to openbox, my brightness keys stopped working. I went into my `.config/openbox/rc.xml` file, commented out the setup for the brightness keys already in there, and posted the brightness keybind settings from my friends' config file.
https://github.com/fouric/dotfiles/blob/master/openbox/rc.xml

Because he used sudo in those, I had to:
`sudo visudo`

Below the `%sudo ALL=(ALL:ALL) ALL`, I put the line:

`thallia ALL=(ALL) NOPASSWD: /usr/bin/tee /sys/class/backlight/intel_backlight/brightness`

After that, saving the file got the buttons to work :D

My background wallpaper was goofed due to the nerds and I attempting to fix the scaling on my HiDPI screen. In the terminal, I ran:

`feh --bg-fill /path/to/background/image`

I've attached hotkeys to rofi, terminal, and tons of other things, and hopefully more things to come. I'm going to mess around more later, for now I gotta get started on my next big project. :)

{thallia}
