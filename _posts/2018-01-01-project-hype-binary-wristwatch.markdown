---
layout: post
author: thalliatree
comments: true
date: 2018-01-01 03:10:11+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/01/01/project-hype-binary-wristwatch/
slug: project-hype-binary-wristwatch
title: 'Project Hype: Binary Wristwatch'
wordpress_id: 1658
categories:
- projects
---

Announcing....my newest project!

I've always loved binary since I learned it, it's so simple and intuitive to me. I found this binary wristwatch on a [German website.](https://www.getdigital.eu/Binary-Wrist-Watch.html)

I wanted to buy it right away, but shipping was a chunk of money, as was the watch itself. That was around a year ago.

I re-stumbled upon the link recently and was inspired to design and build my own binary wristwatch.

I built my first mock-circuit based off of Gector's binary clock, to make sure I had the theory right, then ran his code alongside it.

![img_5383](https://thalliatree.files.wordpress.com/2018/01/img_5383.jpg)
Left top LEDs: Hours ^

Left bottom LEDs: Minutes ^

From the side:
![img_5382](https://thalliatree.files.wordpress.com/2018/01/img_5382.jpg)

https://github.com/CaptainGector/arduinoBinaryClock/blob/master/binaryClock.ino

His code was slightly wonky with my setup. The Hours displayed fine, but the Minutes were off. It took me a while to realize why. When he set up his clock, he had a display like so:

![](https://images.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.b4B7zS1wa1pDwnQl-U6UuQHaER%26pid%3D15.1&f=1)

```
Hours1: 10
Hours2: 0
Minutes1: 30
Minutes2: 7
Seconds1: 40
Seconds2: 9
Displayed Time: 10:37:49
```

Where he had two rows of minutes. In my setup, I'm going for mimicking the wristwatch setup, where there's only one row of minutes and one row of hours.

![](https://www.getdigital.eu/web/getdigital/gfx/productsMore/__generated__resized/1100x1100/Binaere_Armbanduhr_2_Zifferblatt.jpg)

Hours: 8 4 2 1
Minutes: 32 16 8 4 2 1
Displayed time: 12:06

Knowing this, I'm going to have to fiddle with his C code.

Because I'm using a full sized ESP32 for the mock circuit, I ordered a baby ESP32 off of [eBay.](https://www.ebay.com/itm/TTGO-MINI-D1-Wemos-ESP-32S-ESP32-WIFI-Bluetooth-ESP8266-CP2104-Module/401447548021)

The ESP32-S is now out of stock, so I got mine just in time :) Though I derped a little bit in the thinking-things-through process.

Because I only ordered the ESP32-S, I'm going to have to design my own PCB to use it. This is slightly inconvenient because I've never done such a thing, but it's also a really good opportunity to learn how, so I figure it'll be alright to exchange a fast building process for a learning experience.

List of internals:
* Surface mount components (resistors, 74HC595 shift registers, ESP32-S, tactile buttons)
* Power (I'm thinking a coin cell battery? That should be an easy and small thing to manage.)
* Flat top 3mm LEDs

I'm also going to need to design and 3D print a case for this thing, which means learning fusion360 or SolidWorks.

I'm brainstorming the following for the design of the outside container/attachments:

* comfortable, maybe slightly arched on the wrist
* engraved with a circuit design on top
* slim, not bulky. Just big enough to fit the PCB and slip right over the LEDs.
* woven leather or woven wires to attach to the wrist

I'll update as I continue work on this project, learning EagleCAD/KiCAD, fusion360, and much more. :)

{thallia}
