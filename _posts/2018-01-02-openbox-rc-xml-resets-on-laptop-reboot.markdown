---
layout: post
author: thalliatree
comments: true
date: 2018-01-02 23:02:27+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/01/02/openbox-rc-xml-resets-on-laptop-reboot/
slug: openbox-rc-xml-resets-on-laptop-reboot
title: Openbox rc.xml resets on laptop reboot
wordpress_id: 1662
tags:
- config
- openbox
- til
---

My glory days in enjoying openbox were over when I accidentally rebooted my computer. Once logged back in, my openbox configurations had vanished. Tint2 and the others were still there, so I knew it was only openbox that was derping.

As it turns out, the way you log in to your Openbox profile makes all the difference. Who knew?

According to these two forum posts,

[https://bbs.archlinux.org/viewtopic.php?id=85966](https://bbs.archlinux.org/viewtopic.php?id=85966)
[https://bbs.archlinux.org/viewtopic.php?pid=129322](https://bbs.archlinux.org/viewtopic.php?pid=129322)

I had somehow switched between users with `su user`, and forgot about being stuck in the root user profile. To make sure, running `whoami` returns what user profile you're executing commands from.

`su - thallia` got me back to my regular profile and rebooted.

All the keybindings I had bound and desktop setups I had done were disabled...because again, I had set them up in the root profile.

`ls`ing in my `.config/openbox` directory only returned `autostart*`, so the rc.xml I spent so long to configure was wiped, or possibly back in the root profile directory. I decided to just `cp` the template rc.xml file from `/etc/xdg/openbox` to `~/.config/openbox`, and reconfigure everything.

Now in the main user profile, I `cd`'d back into my `~/.config/openbox/rc.xml` and reconfigured everything to my liking, fixed my brightness keys, scaling, keybindings, and desktops.

Rebooting doesn't touch the rc.xml anymore, and my configurations stay put. Just in case, I backed up my rc.xml to my github, because one can never be too careful.
