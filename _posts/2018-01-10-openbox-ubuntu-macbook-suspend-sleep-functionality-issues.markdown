---
layout: post
author: thalliatree
comments: true
date: 2018-01-10 00:07:46+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/01/10/openbox-ubuntu-macbook-suspend-sleep-functionality-issues/
slug: openbox-ubuntu-macbook-suspend-sleep-functionality-issues
title: Openbox/Ubuntu Macbook Suspend/Sleep Functionality Issues
wordpress_id: 1667
post_format:
- Quote
---

Using a macbook with Ubuntu has some really weird side effects that are slightly different than using Ubuntu on a thinkpad or a different PC.

One of these side effects is the lovely, "close the lid, computer sleeps, computer wakes up over and over, computer overheats and loses battery life", which is extremely fun to manage when you don't understand why it's even going on. It's like managing a baby.

First things first. I used the keybindings fouric had in his `rc.xml` file. Those didn't work out too great for me, first of all because `pm-suspend` logged me completely out of my account and I had to search the `f1-f12` to find the desktop again. Annoying. I didn't want to do that every time I put my computer to sleep.

I searched around for other "sleep" functions on Ubuntu, and lo and behold, my lifesaver xautolock appeared before my very eyes.

After a glance through the [man page](https://www.mankier.com/1/xautolock.1x#-version), I knew my mission had begun.

A long time ago I cloned i3lock onto my computer but never finished configuring it, so I decided to mash the two lock thingies together. Because why not? Makes it look *way* cooler than the official openbox lockscreen.

Fiddling around in the command line got me to this finished command:

`xautolock -time 5 -locker "i3lock -i /home/thallia/Pictures/i3lock.png"`

That will wait for 5 minutes of inactivity, then lock my screen with a picture from my computer. I placed that in my `.config/openbox/autostart` file to execute on boot.

But what about launching on command? What if I wanted a keybinding that automatically started that without having to wait?

In `rc.xml`, I made a keybinding for `C-F8` to execute:
`xautolock -locknow`

That command depends on an already running xautolock, which is why I placed the timed one in my autostart, so that'll always be ready and waiting for inactivity or a manual signal.

Now that that had been fixed, the last thing was the annoying "close the lid, wake up, sleep, wake up, etc".

Turns out there's a weird USB signal in /proc/acpi/wakeup that signals the computer to keep waking up after sleep is invoked. Fixed that with a quick,

`echo XHC1 > /proc/acpi/wakeup`

That command gets reset on reboot, though, so I'll put it in a file somewhere to execute on startup.

(EDIT: 2-14-18  I got [i3lock-color](https://github.com/PandorasFox/i3lock-color)! It simply replaces i3lock, and it's way more configurable. For documentation, here is that command:

```
xautolock -time 10 -locker "i3lock -t -i /home/thallia/Pictures/dragon3.png --clock --timecolor=000000FF --datecolor=000000FF --datestr='%Y, %m %A' --insidecolor=a8b9d205 --ringvercolor=6dabff05 --insidevercolor=909cab50 --ringwrongcolor=ff000050 --insidewrongcolor=ff000040 --ringcolor=00000080 --keyhlcolor=ffffff50 --bshlcolor=ffffff50 --time-font=inconsolata --date-font=inconsolata --datesize=16 --radius=110 --veriftext='hack initiated' --wrongtext='t-rekt'"
```

{thallia}
