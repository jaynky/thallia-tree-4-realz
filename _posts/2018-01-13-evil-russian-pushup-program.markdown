---
layout: post
author: thalliatree
comments: true
date: 2018-01-13 06:45:40+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/01/13/evil-russian-pushup-program/
slug: evil-russian-pushup-program
title: Evil Russian Pushup Program
wordpress_id: 1669
post_format:
- Quote
---

Two weeks ago, I challenged myself to complete what is called the "[Evil Russian Pushup Program](https://www.royalmarines.uk/threads/the-evil-russian-pushup-program.3968/)".

This is a bodily intensive, fun workout to keep you moving during the day and strengthening your muscles at the same time. I recently acquired a dress that shows off the shoulders and arms, so I figured it would be a good idea to tone myself up before I wear it to the next folk or swing dance.

This program can be time intensive on certain days, and hardly so on others. Even if you have a packed schedule, I highly recommend trying it out.

Here's the layout:

**WEEK 1**

Mon: 100% test, relative intensity (RI) 30% Set frequency (SF) 60 min
Tues: RI 50% SF 60 min
Wed: RI 60% SF 45 min
Thurs: RI 25% SF 60 min
Fri: RI 45% SF 30 min
Sat: RI 40% SF 60 min
Sun: RI 20% SF 90 min

**WEEK 2**

Mon: 100% test RI 35% SF 45min
Tues: RI 55% SF 20 min
Wed: RI 30% SF 15 min
Thurs: RI 65% SF 60min
Fri: RI 35% SF 45 min
Sat: RI 45% SF 60 min
Sun: RI 25% SF120 min

**WEEK 3**

Mon: 100% test

On Mondays, or whichever day you choose to start, test yourself to your max. Do as many push-ups as you can before muscle give-out. Count how many you have, and that will be your base number of push-ups. Every day, take the percentage and apply it to your base number. This percentage will be the number of push-ups you do every time chunk, so for Monday, if my base push-up number was 45, the percentage is 30%. Take 30% of 45 (which is 14), and perform 14 push-ups every 60 minutes.

It may not seem like a lot of work at first, but trust me, each rep gets more exhausting on the more frequent days, and you really do build muscle. Over the two weeks, I increased my base number of 40 push-ups to being able to do 55 push-ups before fail after the entire program (I got sick a few days into week two and wasn't able to complete three of the days, but my max during week two jumped up to 80). I lost a few inches on each arm, and generally feel stronger.

After a three day rest, I'm going for it again. I challenge you to do the same!

source: https://www.royalmarines.uk/threads/the-evil-russian-pushup-program.3968/

{thallia}

P.S. I now have a discord server where we discuss and problem-solve tech, engineering, and programming things. If you like that, come join us and create a digital hackerspace community :)  [Join here!](https://discord.gg/sCkCkyw)
