---
layout: post
author: thalliatree
comments: true
date: 2018-01-23 19:38:09+00:00
title: ZNC - IRC bouncer on FreeBSD 12.0-CURRENT
---

IRC is great, but  only when you can actually see the messages after you've been away for a long time. If you don't have something like an IRC bouncer set up, it's likely that your computer either disconnects from the server, or you logout/reboot/etc and everything resets.

Annoyinggg.

I decided to get ZNC set up on my server to help me with that problem.

ZNC is an IRC bouncer, it acts like the middle-man between you and the IRC server. ZNC is always connected to the IRC server with your nickname, so the session is always active. You just have to connect to it from your computer to see messages you've missed, so it keeps all the history for you!

Since I'm using FreeBSD, all I had to do `cd` into `/usr/ports/irc/znc` and run,

```
make config
make install clean
```
These two commands take input at various times, so make sure to be there to fill the questions out. Other than that, they just take a large amount of time to install. Be patient!

Once that's completed, go ahead and run, `znc --makeconf`

That should start generating the configuration. If it quits on you with the error of an address boundary error, just run the previous command again.

There are quite a few questions it'll ask you:

```
[ ** ] -- Global settings --
[ ** ]
[ ?? ] Listen on port (1025 to 65534): 6697
[ ?? ] Listen using SSL (yes/no) [no]: no
[ ?? ] Listen using both IPv4 and IPv6 (yes/no) [yes]: yes
[ .. ] Verifying the listener...
[ >> ] ok
[ ** ] Enabled global modules [webadmin]
[ ** ]
[ ** ] -- Admin user settings --
[ ** ]
[ ?? ] Username (alphanumeric): thallia
[ ?? ] Enter password:
[ ?? ] Confirm password:
[ ?? ] Nick [thallia]: thallia
[ ?? ] Alternate nick [thallia_]: thalliatree
[ ?? ] Ident [thallia]:
[ ?? ] Real name [Got ZNC?]:
[ ?? ] Bind host (optional):
[ ** ] Enabled user modules [chansaver, controlpanel]
[ ** ]
[ ?? ] Set up a network? (yes/no) [yes]: yes
[ ** ]
[ ** ] -- Network settings --
[ ** ]
[ ?? ] Name [freenode]:
[ ?? ] Server host [chat.freenode.net]:
[ ?? ] Server uses SSL? (yes/no) [yes]: yes
[ ?? ] Server port (1 to 65535) [6697]:
[ ?? ] Server password (probably empty):
[ ?? ] Initial channels: #utw
[ ** ] Enabled network modules [simple_away]
[ ** ]
[ .. ] Writing config [/home/thallia/.znc/configs/znc.conf]...
[ >> ] ok
```

At the end it'll ask you whether or not you want to start ZNC. Enter `yes`, and, if you happen to have IRC up and running on another computer, you should see your nickname join the channel. :)

Next step is to connect directly to your ZNC server, which isn't difficult.

In weechat, the correct way is:

`/server add znc <server-IP>/6697`

`/connect znc`

If all went well, ZNC should ask you to input your username and password, and viola! Success!

here are some resources if you get stuck along the way:

[http://wiki.znc.in/FAQ](http://wiki.znc.in/FAQ)

[http://wiki.znc.in/ZNC](http://wiki.znc.in/ZNC)

[http://wiki.znc.in/Configuration](http://wiki.znc.in/Configuration)

{thallia}

P.S. I have a discord server where we discuss and solve problems like this! Come join us and create a digital hackerspace community :) [Join here!](https://discord.gg/sCkCkyw)
