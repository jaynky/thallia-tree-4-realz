---
layout: post
author: thalliatree
comments: true
date: 2018-01-29 05:29:42+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/01/29/code-part-1-binary-watch/
slug: code-part-1-binary-watch
title: "Code - part 1 : Binary Watch"
wordpress_id: 1702
post_format:
- Quote
---

The code for the binary watch project can be found on my [github](https://github.com/thallia/BinaryWatch)!

I wrote it basically off of Gector's code, so a bit of copying there, but it really helped me in understanding C/C++ code a little more. I'd like to stick to the C side in general, but the Arduino IDE only accepts C++ .

I had a goofy problem where the minute strip of LEDs were only counting up until 28, when they were really supposed to go to 60. Uhhhh. What?

Turns out the tick function in my code:

```
static unsigned long lastTick = 0;

if(millis() - lastTick >= 1000){
lastTick = millis();
second++;
}
```

was goofed. This was the original:

```
tick = millis();
if(millis() - tick >= 1000){
second++;
}
```

So, for one, tick was created much earlier in the file as a global variable, rather than a local variable within the `loop()` function.

If tick is consistently the same as the return value of the function `millis()`, it will hypothetically always be a random number that you'll get out. This erratically changed whether `second` increased or not, or it constantly increased depending on how long the program had been running.

Once I changed the code to the first example, everything was right in the world again. Now that I have the base code working without issues, I can start to research low power and deep sleep modes and how to implement them with buttons.

{thallia}
