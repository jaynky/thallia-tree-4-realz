---
layout: post
author: thalliatree
comments: true
date: 2018-01-31 06:08:00+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/01/31/code-part-2-binary-watch/
slug: code-part-2-binary-watch
title: "Code - Part 2 : Binary Watch"
wordpress_id: 1704
post_format:
- Quote
---

![](https://thalliatree.files.wordpress.com/2018/01/img_5512-1.jpg)Woot woot! The code is virtually finished, for the simple processing and buttons part at least. :)

I've been utilizing the Arduino Uno this whole time since it's easy to program really fast, that way I could corner all the problems in the code so when I switch to the plain ATmega itself I'll be able to distinguish between what is a hardware issue and what's a software issue.

Since last time, I've still been having issues with some of my functions. The `tick` problem solved some of what was going on, until I added the buttons into play.

I connected the buttons like I configured the input pins to work; Pin 7 was the inHour button, Pin 8 was the inMinute button. I connected one side of the buttons to the input pin and the other side to the 3.3V power supply, enabled the functions, and reloaded the code.

Ack.

The hours and minutes started cycling too fast, and the minutes all of a sudden only counted to 13. What??

I fiddled around with the code for a while, commenting out one function and seeing if it would work on its own, commenting out the other function, you get the idea.

Eventually I caught a few syntax errors and got the functions working by themselves, but enabling both `checkHour` and `checkMin` at the same time resulted in much failure.

I talked to Gector about this, and he suggested pull-up resistors.

I learned that it is unwise to enable a pin as input without resistors like such, and here's why.
When you enable a pin as input in your code and leave it, it joins a state of _high impedance_, otherwise known as a floating pin. This pin picks up noise from around the circuit and can interfere with pretty much everything.Pull-Up or pull-down resistors, when used, filter out this noise, but are low enough resistor values that the input pin can still detect when you press the button (close the switch, etc).

I looked into how to set up these so called "pull-up resistors", and lo and behold, the ATmega328P already has them built into it's microcontroller! Super nice.

I went into my code, and instead of defining my pins like this:

`pinMode(inHour, INPUT);`
`pinMode(inMinute, INPUT);`I defined them like this:

`pinMode(inHour, INPUT_PULLUP);`
`pinMode(inMinute, INPUT_PULLUP);`Then, in my functions where I had previously written the pins to expect a HIGH input, I changed that to LOW.
I did this because pull-up resistors "pull" the voltage of the input pin up, so the pin is HIGH, and only triggers when it receives a LOW signal. The inverse is true when talking about pull-down resistors. Pull-down resistors enable the pin as LOW, and the pin waits for a HIGH signal.

After reloading the code and making sure I had the correct circuit setup with the buttons, I tested them out.

![img_5510](https://thalliatree.files.wordpress.com/2018/01/img_5510.jpg)

AND THEY WORKED.

*drops the mic*

{thallia}
