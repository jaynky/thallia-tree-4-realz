---
layout: post
author: thalliatree
comments: true
date: 2018-02-02 08:12:33+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/02/02/tint2-replacement-polybar/
slug: tint2-replacement-polybar
title: 'tint2 replacement # polybar'
wordpress_id: 1715
post_format:
- Quote
---

tint2 is a taskbar that you can configure your window manager, typically with openbox, to have a bar at the top of your screen that displays info  about tabs, battery, and date and time for you.

While tint2 is great and all...it is by no means pretty. It's janky to set up, and looks pretty janky too, which is a no no for me.

My friend mentioned he had set up polybar, and I remembered I had had it bookmarked for the longest time to get it set up, so I decided to give it a shot!

First, I cloned the repository onto my computer:

`git clone --branch 3.1.0 --recursive https://github.com/jaagr/polybar`

And, following the instructions,

```
mkdir polybar/build
 cd polybar/build
```

Once here, I recommend installing all the dependencies first to make sure you don't get a bunch of errors in a row.

```
sudo apt install cmake cmake-data libcairo2-dev libxcb1-dev libxcb-ewmh-dev libxcb-icccm4-dev libxcb-image0-dev libxcb-randr0-dev libxcb-util0-dev libxcb-xkb-dev pkg-config python-xcbgen xcb-proto libxcb-xrm-dev i3-wm libasound2-dev libpulse-dev libmpdclient-dev libiw-dev libcurl4-openssl-dev libxcb-cursor-dev
```

That should do the trick, and supply you with support for allllll the features!

Once that's done, `cmake ..` in the build folder, then `sudo make install`.

And boom, you're done!
To configure polybar, create a `config` file in `~/.config/polybar/`.

After that, you can configure polybar with the modules and colors you'd like to your heart's content! Here's my config file: https://github.com/thallia/dotconfig/polybar/config

and what it looks like:![screenshot](https://thalliatree.files.wordpress.com/2018/02/screenshot1.png)

to test out your config, you can execute `polybar example`.

{thallia}

P.S. I have a discord server where we discuss and solve problems like this! If you're having trouble setting up your config or want to share what your cool config looks like, we'd love to see it! Come join us and create a digital hackerspace community :) [Join here!](https://discord.gg/sCkCkyw)
