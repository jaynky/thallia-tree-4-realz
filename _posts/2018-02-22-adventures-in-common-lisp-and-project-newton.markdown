---
layout: post
author: thalliatree
comments: true
date: 2018-02-22 23:39:42+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/02/22/adventures-in-common-lisp-and-project-newton/
slug: adventures-in-common-lisp-and-project-newton
title: Adventures in Common Lisp and Project Newton
wordpress_id: 1763
post_format:
- Quote
---

Goooood morning!

In the last few days I have taken on the challenge of learning Python and Common Lisp at the same time. My experiences in programming have been little, only writing some small programs in Assembly or Forth here and there, so this has been a big jump for me.

I'm learning Python because my friend and I wanted to write a scratchpad bot for our discord server - a bot that will grab the last message (or however many messages you specify) and put that message in a JSON file for you to reference later. in [UTW](https://thalliatree.wordpress.com/under_the_wire/),  we share ideas, commands, programming techniques and more, so this is a very helpful tool. I'll get a post up about him and the things I learned while designing him soon.

As for Common Lisp, I _love_ lisp. It's much less choppy than Python feels, and very intuitive to me. It's similar to forth, in a way, but obviously more documented and widely used.

Documentation of things I have learned about Lisp:

Parentheses. Everywhere. It's beautiful.

The function `values()` returns multiple values, not a _list_ of values. It'll return the actual values and won't break your code if not all of them are used!

`&optional` is a keyword you can put into the arguments part of a function to tell the function that these arguments are optional. If no value is provided, you can set the argument to a default value:

`(defun foo ($optional (bar "Hello World")) (print bar))`

This creates a function `foo` that'll take an optional argument `bar`. If you pass `bar` to the function, bar will become that value. If nothing is passed to `foo`, `bar` will print "Hello World".

the function `clear-input`, as I understand it, cleans away any stray or unprocessed input, making sure it's all inputted, before it starts executing the next code.

`finish-output` makes sure everything successfully printed to the output-stream and returned.

`write-string` is the same as `write-line`, except for `write-line` adds a newline before it prints.

`parse-integer` will take input and evaluate it as an integer, then return the value. Or throw an error if the input isn't an integer.

I also learned about `asdf systems`, which are _super_ useful! ASDF stands for Another System Definition Facility. It is for programmers and hackers to build and load software in Common Lisp. So all of your files that depend on each other (in a project, for example), you can load them in specific order dependencies based on a `project-name.asd` file and load it into SBCL to test and run things, working similar to how you would load a required package with quicklisp.

[What is ASDF](https://common-lisp.net/project/asdf/)

[Getting Started with ASDF](https://common-lisp.net/%7Emmommer/asdf-howto.shtml)

And with that, I can sort of announce the Lisp project I'm working on! It's called Newton, which I started to help teach me lisp.
Newton is a Physics calculator, dictionary, concept-teacher, reference, you name it. Like a mini teach-yourself-phyics program! It's been lots of fun so far, you can see my ASDF System file for Newton here:

https://github.com/thallia/newton/blob/master/newton.asd

The actual code is _far_ from complete, I'm still extensively experimenting with the little bits of lisp that I know.

Until next time!

{thallia}
