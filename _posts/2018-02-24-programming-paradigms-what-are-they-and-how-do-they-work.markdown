---
layout: post
author: thalliatree
comments: true
date: 2018-02-24 08:06:24+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/02/24/programming-paradigms-what-are-they-and-how-do-they-work/
slug: programming-paradigms-what-are-they-and-how-do-they-work
title: "Programming Paradigms : What Are They and How Do They Work?"
wordpress_id: 1769
post_format:
- Quote
---

(disclaimer: this is a beginner's explanation and understanding of these programming paradigms. if there are any errors or confusion, please let me know and I will edit or correct those to become more understandable. this is intended to be a basic overview.)

In my quest of self taught programming, I figured I've reached the point of needing to really understand what an object is, as well as the difference between the big types of programming:



 	
  * Object Oriented Programming (OOP)

 	
  * Functional Programming (FP)

 	
  * Structured Programming (SP)

 	
  * Imperative Programming (IP)

 	
  * Procedural Programming (PP)

 	
  * Declarative Programming (DP)

How do these all relate? What are the pros and cons of using each one? I decided to go on a researching rampage.

Let's clarify some terms. What is a paradigm?

 	
  * n. A set of assumptions, concepts, values, and practices that constitutes a way of viewing reality for the community that shares them, especially in an intellectual discipline.

 	
  * n. A system of assumptions, concepts, values, and practices that constitutes a way of viewing reality.

 	
  * n. A conceptual framework—an established thought process.

So in our case, a paradigm is a way of looking at programming: understand the underlying framework of the language's ideas that make it work the way it does and implementing programs through the filter of that structure.

As [wikipedia](https://en.wikipedia.org/wiki/Programming_paradigm) says, "a programming paradigm is a way to classify a programming language based on the features it possesses.", filtering the abilities through a set of ideas to classify it. Programming languages can share qualities/features that tie into multiple paradigms, so there isn't one strict classification for each language.

"Some paradigms are concerned mainly with implications for the execution model of the language, such as allowing side effects, or whether the sequence of operations is defined by the execution model. Other paradigms are concerned mainly with the way that code is organized, such as grouping a code into units along with the state that is modified by the code. Yet others are concerned mainly with the style of syntax and grammar." ~ wiki



* * *



**Structured Programming **

Structured programming focuses specifically on _program organization. _The programs are typically executed in the exact format it is laid out in, from beginning to end, using statements. There are four terms usually associated with structured programming:

**Sequence**: The order in which to execute the program, often a linear format of statements.

**Selection:** Based on the state of the program (local and global changes), statements are executed (Statements such as if/then/else/else-if).

**Iteration**: Iteration in the sense of SP has to do with a statement repeating or being executed until the desired program state is reached. (for/do, while, etc).

**Recursion**: The program repeatedly calls itself until the desired state of the program is reached or something external terminates it.

Structured programming can be implemented in any language. It is often used for small, clear programs like OS system interrupts or external interrupts.



* * *



**Procedural Programming**

Procedural programming is defined by a concept called "procedure calling". This is similar to functions, but the term focuses on different features.

Procedures can be functions, routines, subroutines, and more. It's an overarching term for "a thing that does a series of other little things, kept in a box that you can call and open, filter a thing through, then close and put away".

Some programming languages classified as "procedural" are C, Fortran, Go, and BASIC. Hardware is designed to support these languages with the stack register(s) (a register that holds all the calls, pointers, and organizes the program) to make sure that things are called correctly, and return successfully before moving onto the next procedure.



* * *



**Imperative Programming**

Considering a language as imperative is classified as programming with statements to change the state of the program. It's focus is to describe _with the commands_ how the program works. It is concerned with the process of getting to the output, not necessarily the output itself.

The idea is similar to imperative statements in the English language: if you say something imperatively, it is a command, something that implies you want a thing done. Translating this to programmer terms, you're literally commanding the computer to do things.

To quote [wikipedia](https://en.wikipedia.org/wiki/Imperative_programming), "imperative programs consist of _commands_ for the computer to perform".

Imperative programming is linear, which means you tell the computer every. little. step. you. want. it. to. take, and that's how it will be executed. You focus directly on how you're instructing the computer to do things. Lower level code in C or Assembly is considered imperative programming.

Contrary to usage, procedural programming and imperative programming do not reference the same thing. While _imperative programming_ focuses on _how_ you're telling the program what to do, the structure you're writing it in, _procedural programming_ is focused on the idea of _calling_ those procedural "boxes" and executing them.
Procedural programming is a sub-classification of imperative programming, because you'll likely use the imperative code to _write_ the procedures you want to call. "[T]he use of procedures has a dramatic effect on how imperative programs appear and how they are constructed." ~ [wiki](https://en.wikipedia.org/wiki/Imperative_programming)



* * *



**Declarative Programming**

Declarative programming is the opposite of imperative programming, it is defined as "anything _not_ imperative". The languages considered as so typically associate with mathematical logic, rather than explicitly listing the steps you want the computer to perform. The languages defined as declarative often have the ability to support no side effects (modifying things like variables outside of the function's scope, which depends on program state).



* * *



**Functional Programming**

Functional programming is a sub-classification of declarative programming. It is a style of programming that "treats computation as the evaluation of mathematical functions (expressions) and avoids changing-state [side effects] and mutable data." ~ [wiki](https://en.wikipedia.org/wiki/Functional_programming)

Imagine we have `function`, and we passed it `(x)`. Calling `function` twice with the same input value for `(x)` will result in the same return value of `f(x)`_each time_, as opposed to having the output possibly change depending on a program's state (due to a local or global variable).

This makes it much much easier to predict the outcome or behavior of a program, because you have more clarity on the flow of the data, you know what to expect.

Functional programming is used in programs where you have a fixed set of data, but need to add new operations every once in a while to process it differently. Languages like Haskell, Common Lisp, Scheme, Clojure, and Erlang all support functional programming.

Functional programming has some powerful implementations, like pure functions and high order functions. I don't understand them well enough to explain them here, so go and do some research for yourself!



* * *



**Object Oriented Programming**

Lets clarify what an object is: Objects are a thing that have a _state_ and a _behavior._ If you take a look at literal objects around you:

Phone:
My phone's state's can be On or Off, bright or dim, silent or ringing.
My phones behavior can increase volume, decrease volume, web surf, search, play music, pause music, send call, hang-up call (etc).

Lamp:
Lamp's state: On, Off
Lamp's behavior: turn on, turn off, (maybe even explode!)

Pencil:
State: mechanical (or wood), lead (vs ink), erase, don't erase
Behavior: write, delete/erase, refill, click

An object in programming stores its _states_ in fields (the storage of the data) and displays its _behavior_ with methods or functions (the activity that will be performed on the data it receives).

Object Oriented programming focuses on data, where you have fixed methods/operations that you pass new data to. Programs with OOP are rooted in the creation and interaction of objects themselves as they sift through the ever-changing data passed to them.

Languages that support OOP or are designed specifically for OOP: Java, C++, C#, Python, PHP, Ruby, Perl, Swift, Dart, Scala, Common Lisp, and SmallTalk.

Objects are extremely useful if implemented correctly. You can imagine objects as the nuts and bolts that, when built, work together as a machine. If the machine breaks, you replace one part, not the whole machine.

Object oriented programming has one more term that is worth mentioning, called data-encapsulation. Think of objects like the human body and it's makeup. There are all the internal organs inside, but we can't access them unless we do specific things. Say we wanted to access the stomach, to do so, we would eat food. We inputted the food, it goes through the stomach, is processed, and I'm sure you can imagine what the output is.

Did we see on the inside everything that was happening to the food? Nope! We passed an argument, and got a value returned. Data encapsulation is the idea of hiding the _state of the data_ until it is _returned_ by the object.



* * *



I hope this was educational to you, as it was to me! Now I fully understand more what people mean when they reference these terms, and hopefully I can start implementing these ideas into my own programs.

If you have any questions, don't hesitate to contact me and ask more. Corrections and clarifications are welcome!

{thallia}
  *[n.]: partOfSpeech
