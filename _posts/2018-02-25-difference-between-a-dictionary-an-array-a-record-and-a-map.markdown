---
layout: post
author: thalliatree
comments: true
date: 2018-02-25 02:48:10+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/02/25/difference-between-a-dictionary-an-array-a-record-and-a-map/
slug: difference-between-a-dictionary-an-array-a-record-and-a-map
title: Difference between a Dictionary, an Array, a Record, and a Map
wordpress_id: 1772
post_format:
- Quote
---

In programming, there are different structures available to sort your data.

These structures can look like a list, a dictionary, array, record, map, and I'm sure there are more that are slipping my mind at the moment. What distinguishes all these from each other?



* * *



**Array**

An array is a linear list of data that is indexed. You can access the chunks of data by referencing the array, and the part of memory the data is stored in.
Arrays are linearly ordered, so it keeps its index structure. Arrays hold homogeneous data, meaning it can only hold one data-type at a time.
You could have an array of strings, or an array of integers, but you can't have an array that holds integers _and _strings. Certain programming languages, like common lisp, allow arrays to have heterogeneous (different) datatypes, whereas other programming languages are strict about having the same datatype.
Arrays can be fixed-length or resizable.



* * *



**Record**

A record differs from an array because they must be a fixed number of fields, the fields each have a name, and the fields can hold different datatypes. There is no general order to the record, so the fields are accessed by name.

You could have a record named "Friend" that holds someone's name, age, and date of birth. Then you could create an array to hold the record. A record's values are called "members".



* * *



**Dictionary**

A dictionary is a data structure that organizes data in key:value pairs. You access the value from the dictionary by identifying/passing the key, which is searched for in the dictionary, then the value matching the key is returned.

You could have "name" : "thallia", "hobby" : "coding". You could even have a dictionary within a dictionary, with sub-key/pair values depending on the sort of data you need to store.

The Map is, according to my research, virtually the same as a dictionary. Certain languages "name" dictionaries Maps, and certain languages say maps are "dictionaries". They can be used synonymously, but that is typically avoided because the term "maps" is found in [functional programming](https://thalliatree.wordpress.com/2018/02/24/programming-paradigms-what-are-they-and-how-do-they-work/), which references high-order functions.

Happy learning!

{thallia}
