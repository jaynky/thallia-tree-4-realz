---
layout: post
author: thalliatree
comments: true
date: 2018-03-01 03:31:34+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/03/01/i3wm-setting-up-backlight-and-sound/
slug: i3wm-setting-up-backlight-and-sound
title: i3wm Setting up backlight and sound
wordpress_id: 1777
post_format:
- Quote
---

When moving to i3 window manager, my backlight and sound keys got disabled. This is a super simple fix.

Get to your i3 config in `.config/i3/config`, and enter these lines for the backlight:

```
# audio
bindsym XF86AudioRaiseVolume  exec --no-startup-id pactl -- set-sink-volume 0 +5% 
bindsym XF86AudioLowerVolume  exec --no-startup-id pactl -- set-sink-volume 0 -5%
bindsym XF86AudioMute         exec --no-startup-id pactl set-sink-mute 0 toggle
```
and enter in these for brightness:

```
bindsym XF86MonBrightnessUp exec "bash -c 'expr $(cat /sys/class/backlight/intel_backlight/brightness) + 100 | sudo tee /sys/class/backlight/intel_backlight/brightness'"
bindsym XF86MonBrightnessDown exec "bash -c 'expr $(cat /sys/class/backlight/intel_backlight/brightness) - 100 | sudo tee /sys/class/backlight/intel_backlight/brightness'"
```

Viola! Everything should work now :)

{thallia}

P.S. I have a discord server where we discuss and solve problems like this! Come join us and create a digital hackerspace community :) [Join here!](https://discord.gg/sCkCkyw)
