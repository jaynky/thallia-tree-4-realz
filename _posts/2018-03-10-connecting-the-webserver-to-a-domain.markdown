---
layout: post
author: thalliatree
comments: true
date: 2018-03-10 04:04:44+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/03/10/connecting-the-webserver-to-a-domain/
slug: connecting-the-webserver-to-a-domain
title: Connecting the Webserver to A Domain
wordpress_id: 1789
post_format:
- Quote
---

Over the past week I have dedicated an insane amount of hours to getting this wordpress blog ported to my other domain. To do this, I had to set up a web server and redesign the website in HTML/CSS format.

To do this, I didn't want to have to learn HTML, so I use a "wysiwyg" What You See Is What You Get HTML site generator, where you do all the design and it generates HTML code for you. I used [BlueGriffon](http://bluegriffon.org/), and by far a great tool than some of the other wysiwyg things I found out there. BlueGriffon has a fairly smooth user interface, and an easy way of configuring things to the way you want them.

I used [the Site Wizard's](https://www.thesitewizard.com/bluegriffon/bluegriffon-2-tutorial-1.shtml) tutorials on how to use BlueGriffon to generate a fully functioning website. His stuff is super thorough, so if you have absolutely no technical knowledge at all you'd be able to follow his stuff very easily.

I figured the best way to share files between my server and computer for now would be a [GitHub repository](https://github.com/thallia/thalliastree). This repo stores all my sites files, so I can edit them on my computer, push to the repo, and pull on the server to put the changes in effect. Supes ezpz.

Now, to actually get the domain working with my server:

I didn't have git or tmux installed, so that was the first pit stop. Since I'm using FreeBSD 12.0-CURRENT, there are no packages that are updated for the OS (since it's a developers version, you have to install _everything_ by hand. Even the package manager.

Anyway, to install git, `cd /usr/ports/devel/git`
Then:
`sudo make`
`sudo make install`
`sudo make clean`

The next step was to fix the `DocumentRoot` in two files, so my domain knows where to access the site files.

First, change `DocumentRoot` in `/usr/local/etc/apache24/httpd.conf`,

I git cloned `thalliastree` into apache's data repo, so the path I put in `DocumentRoot` was:

`DocumentRoot       /usr/local/www/apache24/data/thalliastree/`

Also update the `<Directory "path/here">` thing to have the same path as DocumentRoot.

Inside the `<Directory "/usr/local/www/apache24/data/thalliastree/">` code block, you need to specify the initial index file that will pop up on your screen when someone accesses your site, which looks like this:
`DirectoryIndex index.html`

Where index.html is the homepage file for your site.

We need to update the path of one more file:

`sudo vim /usr/local/etc/apache24/sites-available/bsd.lan-ssl.conf`

Find the `DocumentRoot` line, then update the path to be the same as the one you updated earlier.

After that, everything should be ready to go, if you followed my other blog post: [Setting up A Web Server on FreeBSD 12.0-CURRENT](https://www.thalliatree.net/posts/web-server-freebsd-12-current.html)

Run `sudo service apache24 restart` on your webserver, direct a browser to your domain, and viola! Your newly designed website should appear :)

I had a problem where I couldn't access my website from inside my network, which was partly fixed by adding an entry in `/etc/hosts`.

Now that this is all over, I can debut my new site, stay tuned for a blog post about moving to my newly hosted site :)

{thallia}
