---
layout: post
author: thalliatree
comments: true
date: 2018-03-16 07:10:38+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/03/16/code-part-3-binary-watch/
slug: code-part-3-binary-watch
title: "Code - part 3 : Binary Watch"
wordpress_id: 1793
post_format:
- Quote
---

Last we talked about the Binary watch I was busy finishing up the second rendition of the working code and getting ready to complete the schematic.

As the date I head off to college comes nearer and nearer, the urgency of finishing my projects is heightening! And so I take to Spacemacs to venture through the depths of ATmega328P's sleep modes, RTC modes, and more...

I roughly sketched out a nice schematic on EagleCAD that will soon be posted in my GitHub, but I gotta finish some hardware testing before that happens.

As for the code side of things, the next steps were to research sleep modules and see which ones work with my project set-up the best.

Reading through the datasheet is rather monotonous, it gave me a vague idea of what the sleep modules were, but I still had a hazy view of what I needed to do, so I headed over to the [Arduino Stack Exchange](https://arduino.stackexchange.com/questions/50733/is-there-a-way-to-disable-modules-like-adc-without-a-sleep-mode).

My initial (albeit naive) idea was to keep the CPU, flash, and Clock running while putting all other modules to sleep. I started to test this, but disabling the modules had hardly no effect on the current pull of the ATmega328p, which was another reason I asked on Stack Exchange. One person said,

"You do **not** want to keep the CPU running on a battery-powered project! You should use a timer for keeping the time, and let the CPU sleep. Ideally, you would connect a 32,768 Hz crystal to TOSC1/TOSC2 and have Timer 2 work in RTC mode. I have [a clock](https://www.logre.eu/wiki/Horloge_analogique_24h/en) running since June 2012 on the same pair of AA cells using this low-power technique."

This piqued my interest, so I went to research heavily on what RTC mode was, and how this stuff works alongside a sleep mode.

RTC stands for Real Time Clock, which is typically a 32.768kHz crystal externally connected to TOSC1/TOSC2 or XTAL1/XTAL2 pins. This RTC mode allows the entire microcontroller to sleep, while still ticking the external clock outside the sleep mode. With SLEEP_MODE_PWR_SAVE, the second-to-lowest power saving mode, you can wake it up through either a timer interrupt or a signal on the INT0/INT1 pin.

This seemed like the best solution, but how to implement it?
I have found *tons* of different code and libraries for this, so much so that I just got confused.

After much research and questioning, I came to two conclusions, of which I will try both:

1) Write code to work with an RTC/IC, like the [DS1307](https://learn.adafruit.com/ds1307-real-time-clock-breakout-board-kit/what-is-an-rtc) and code from [this arduino library](https://github.com/MajicDesigns/MD_DS1307), that way I don't have to mess around with setting all the hardware bits and bytes and confusing myself to no end. (more documentation on the arduino library [here](https://majicdesigns.github.io/MD_DS1307/index.html)) It seems as though the DS1307 is a plug'n'play device, as long as you get the code right.

2) [as of 3-15-17 at [23:19](https://youtu.be/cUFJ1yVhJ6g)] Test out [Nick Gammon's response](https://arduino.stackexchange.com/questions/50733/is-there-a-way-to-disable-modules-like-adc-without-a-sleep-mode?noredirect=1#comment103133_50733) on my Stack Exchange question. The code he gave me was very insightful with accomplishing all the bits and bytes and changing the hardware registers.

I have ordered some 32.768kHz crystals and the DS1307 board, both of which should come this Monday. In the meantime, I [typed up some quick code](https://github.com/thallia/BinaryWatch/blob/master/src/bwatch3-0.cpp) to go with the DS1307, and [adjusted the code](https://github.com/thallia/BinaryWatch/blob/master/src/bwatch-clk.cpp) from stack exchange. Both compile normally, so we'll have to see how it works out when the actual hardware arrives. I'm sure mass debugging will entail...

Depending on which one works best for my purposes, I'll adapt the schematic to the design and then start work on the PCB.

{thallia}
