---
layout: post
author: thalliatree
comments: true
date: 2018-05-03 06:56:03+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/05/03/pcb-95-complete-binary-watch/
slug: pcb-95-complete-binary-watch
title: "PCB 95% Complete : Binary Watch"
wordpress_id: 1805
post_format:
- Quote
---

A little under a month ago, my PCBs arrived! It has been a hilariously long wait to get everything else up to par.

My GitHub repo is a little more updated every now and then before I write posts, if you want to follow along as I finish up: [thallia's GitHub](https://github.com/thallia/BinaryWatch)

The binary watch came in the cutest purple package:

![img_5899](https://thalliatree.files.wordpress.com/2018/05/img_5899.jpg)![img_5898](https://thalliatree.files.wordpress.com/2018/05/img_5898.jpg)

And here's the final product!!![img_5897](https://thalliatree.files.wordpress.com/2018/05/img_5897.jpg)

Isn't it gorgeous? I love the purple color.

I went immediately into soldering with the parts I had (which of course, took an extra week to arrive because of weather.)

![img_5906](https://thalliatree.files.wordpress.com/2018/05/img_5906.jpg)

I forgot how tedious SMD/T soldering is.
Finally, I had all the parts soldered except for my lovely ATMega328P-AU.

![img_5936](https://thalliatree.files.wordpress.com/2018/05/img_5936.jpg)

It took me forever to get a programmer for that thing. First I tried soldering wires to it, which was a bad idea.

Next, I tried ordering a cheap board from this place called Schmartboard. They had really good pricing but...![img_5935](https://thalliatree.files.wordpress.com/2018/05/img_5935.jpg)

The place for the TQFP was wayyyyy too small. Oh well. I got a free pair of headphones out of it, so who am I to complain?

I finally got a paycheck where I could order one of these babies:

![img_5990.jpg](https://thalliatree.files.wordpress.com/2018/05/img_5990-e1525330351836.jpg)

And lemme tell ya. It was worth the $3.99 shipping. Not only was it super sleek and easy to solder the headers onto, but it worked ON THE FIRST TRY.

![img_5991](https://thalliatree.files.wordpress.com/2018/05/img_5991.jpg)

That doesn't usually happen, so I was _extremely_ ecstatic.

![img_5992](https://thalliatree.files.wordpress.com/2018/05/img_5992.jpg)

With some handy dandy wires I got the chip programmed, soldered onto the board, and I was finished.

But....the circuit doesn't work on the PCB. So I now get to go through the delights of debugging a PCB, which sounds nightmare inducing and fun at the same time. We'll see! XD But either way, this project is literally 99.9% done! I'm so excited.

In my next post, I'll announce my newest project, the hardware and software I'm using, and what I've been learning to complete it in this post-absence of an Aprilish-May.

{thallia}
