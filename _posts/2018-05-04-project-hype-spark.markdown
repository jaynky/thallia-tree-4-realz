---
layout: post
author: thalliatree
comments: true
date: 2018-05-04 07:04:05+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/05/04/project-hype-spark/
slug: project-hype-spark
title: 'Project Hype: Spark'
wordpress_id: 1816
post_format:
- Quote
---

So far, I have completed all of the goals I wanted to get done before college. This is huge! And now I'm bored...

Aside from debugging my binary watch's PCB of course, but that's minor and doesn't take a ton of intellectually-stimulating brain thinking.

In the past month, I have invested more into my drawing and art-ing skills. I've always loved to draw, but just drawing scenery and characters and chibis have never really filled the gap I wanted it to. I love drawing and the results are always super fun, but I've never known what to actually do with what I produce.

Then...I had a spark.

What if I combined my art and creativity with technology and physics?

Thus was born Spark, an animation project to bring physics concepts to life. For this animation, I'll be focusing on electricity/voltage, V=IR, electrostatic forces, and electromagnetic fields.

I have had a brief history of animation, starting when I was the wee age of twelve. I was fascinated with animation companies and how they produced movies. I would buy the art books of my favorite movies to see the concept art, storyboards, character design, and more. ![img_6006](https://thalliatree.files.wordpress.com/2018/05/img_6006.jpg)

(my collection of art books!) And yes, I have thoroughly inspected each and every one of these. I would pour over them for _hours. _I still do, sometimes!

Back to the point: I love animation. The process, the execution, the art. Gector lent me a laptop a while ago with Krita on it (Krita is an art and animation software), so I got my first taste of animation before the computer deleted my files .-.

But all hope is not lost! My grandma surprised me a week ago and bought me a Huion H610Pro tablet, which was beyond wonderful and I can't be more thankful that she did that for me.

![img_5970.jpg](https://thalliatree.files.wordpress.com/2018/05/img_5970-e1525417095780.jpg)

![img_5971.jpg](https://thalliatree.files.wordpress.com/2018/05/img_5971-e1525417125877.jpg)

Isn't it _pretty_?

I did some configuration on Linux following this guide: https://askubuntu.com/questions/500141/huion-h610-tablet#854842

![img_5972](https://thalliatree.files.wordpress.com/2018/05/img_5972.jpg)

And Huion was easily recognized by my laptop! :D It's seriously like drawing on butter. I love it.

So far, I created a basic script/timeline of how the animation will run, and to do that I'm researching my brain off to learn deeper and deeper about the things I'm explaining.

It is likely I won't get it done super fast right now since I have a lot of schoolwork and work piling on me, but I'm doing my best to keep up with the speed my brain is running at on this project.

I hope you all will be as excited for the next update as I am!

{thallia}
