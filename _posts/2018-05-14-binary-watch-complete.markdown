---
layout: post
author: thalliatree
comments: true
date: 2018-05-14 05:07:10+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/05/14/binary-watch-complete/
slug: binary-watch-complete
title: "Binary Watch : Complete"
wordpress_id: 1824
post_format:
- Quote
---

5 months ago, I'd never designed a project from scratch. 5 months later, I've learned to design a PCB from a schematic, manufacture it, use a barebones microcontroller, 3D model with fusion360, write code that interacts with hardware, and much more. This was a crazy fun project!

*insert fanfare here*

Debugging the PCB was easier than I thought, I realized I was pressing the wrong buttons, therefore the code was running but it wasn't displaying. You would think the designer of the device would know what buttons to press, right? Haha.

Gector (from UTW) bought a 3D printer of his own, so I revamped my 3D-modeled case design and sent it to him to print. ![img_6081](https://thalliatree.files.wordpress.com/2018/05/img_6081.jpg)

Nice and shiny and black. :))

![img_6082](https://thalliatree.files.wordpress.com/2018/05/img_6082.jpg)

Looks preeetty slick!

for![img_6083](https://thalliatree.files.wordpress.com/2018/05/img_6083.jpg)

The PCB fits really well, too.

[https://youtu.be/BdNp0CsC454](https://youtu.be/BdNp0CsC454)

I deem this project officially done, from a designing standpoint! Everything after this will be upgrades, which I will work on when I have the time. With college in the very near future and working 2 jobs, my time will be pretty divvied up between work and family.

But! I have a list of upgrades and things to do next for this, so my mind isn't taking a rest.

For the next board and case revision, I want to upgrade:



	
  * Power - The charging and power circuit needs to be on the board itself. That way I can charge it and not have to worry about buying a bunch of LiPo batteries over and over.

	
  * Experimenting with new power - I want to try powering this thing from a coin cell again. I have 2 other boards with this design, so I'm going to solder them together, but with different resistor types on the LEDs to see how much current the LEDs and microcontroller are drawing. That way I can estimate what the best resistor value is for brightness + mAh value of battery to get.

	
  * The 3D case is cool with the PCB exposed, but it ain't cool because it's exposed to the weather and other wet and sticky things. Therefore I must cover it up. The lid for case I design next will likely be screwed on, 1) for easier access and 2) for better security of the PCB. Depending on how secure I want it, I may add some kind of sealant. We'll see...

	
  * 3D case - If I add a charging port, it'll need a hole for the plug.

	
  * Through-hole LEDs - if the PCB is to be covered by the case, SMD LEDs aren't going to shine through very well. I have some cool cylindrical blue LEDs that should work, so I'll have to design the case cover to sit around/on top of them, which requires a new PCB design entirely.

	
  * With the PCB case covering the top, I'll need to either add holes for the pushbuttons or design a way to put them on the sides of the case.

	
  * Watch strap - I'm planning on weaving some leather cord in a celtic knot fashion, or maybe some cool braided thing. I've been looking at some chunkier weaves on Pinterest, but I haven't found the right one yet.

	
  * With the new case top, I may make it thicker and engrave some cool circuit-looking designs around the LEDs poking through and paint the engravings an electric blue color. Electric blue is a great idea.


And there you have it. This project is finished, and just getting started at the same time!

As for mini updates on other projects: I'm still saving up to replace the hard drive in my dell server (the old one croaked on me), and Spark is still very much in the works. Spark has some script work and content work to be done, but it will be updated soon :)

Onward, hackers!

{thallia}


