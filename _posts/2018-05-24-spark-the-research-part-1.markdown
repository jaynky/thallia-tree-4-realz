---
layout: post
author: thalliatree
comments: true
date: 2018-05-24 03:51:09+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/05/24/spark-the-research-part-1/
slug: spark-the-research-part-1
title: "Spark : The Research Part 1"
wordpress_id: 1833
---

Though exciting, finding the right way to tackle this project has been tough! But I'm taking it in chunks.

As always, Trello is my organization go to! This is my Spark board, with the in-the-works outline, the next steps, the animation to-dos, and what I've done so far. (...I promise I actually have done more than that.)

![screenshot](https://thalliatree.files.wordpress.com/2018/05/screenshot.png)

As it goes, for a project like this there is always things to learn and research to be done! And especially for this one, since there are things that I will be covering in the video/animation that I myself don't fully understand. :D

Here's some of the basic information I've learned so far.

The word _electricity_ is used in so many different ways that I am going to avoid using it as much as possible, though that is the thing I will be attempting to teach. Electricity has many connotations, many different meanings, and is all around confusing when you use it repeatedly to describe contrasting ideas.

A useful link: [http://amasci.com/miscon/whyhard2.html](http://amasci.com/miscon/whyhard2.html)

I assume that the reader understands the basic model of an atom and has a foundation of  Work and Energy concepts. This is the generic highschool definition of voltage, which is about halfway accurate. I’m saving the calculus definition for another blog post by itself.

Disclaimer: Though I mention and talk about V = IR, I will not cover resistance.



* * *



Charge. In the physics world, **Charge** is a physical _property of matter_, similar to how plants have different chemical properties. Particles and matter have the _charge_ property, like the lavender plant has the _calming_ property*, or how matter can also have properties such as mass, temperature, and more.

Of course plants are on a much larger scale than the particles in question, but the analogy still stands.

Charge is measured in **Coulombs**. Protons have a positive charge of +1.60217662 x 10^-19 Coulombs, electrons have a negative charge of -1.60217662 x 10^-19 Coulombs. That is an extremely small number! (0.000000000000000000160217662, to be visually exact.)

Since the charge of 1 electron is just a fraction of 1 Coulomb, how many electrons would it take to equal 1 Coulomb total?

1 ÷ 1.60217662 x 10^-19 Coulombs ≅ _6,241,509,000,000,000,000,000,000_ electrons! (6.241509 x 10^18)

**Protons** and **electrons** are so tiny that it takes trillions of them to equal 1 Coulomb of measurement, as seen above.

In the topic of electricity, we often hear of terms such as **current**, **voltage**, and **resistance**. All three of these terms make up a very important equation we call **Ohm's Law**.

Ohm's Law looks like this: **V**** =**** I**** *** **R**

Where V stands for Voltage, I stands for Current, and R stands for Resistance.

For you non-equational folk, have no fear. I will do my best to explain without mathematics.

Because we know about charge, we can start using our knowledge to flesh out more of these concepts. Lets start with I, Current.

Current is the measurement of how many Coulombs (or charge) is running past a certain point every second. When we look at the unit of Current, it is:

![coulomb-per-sec](https://thalliatree.files.wordpress.com/2018/05/coulomb-per-sec-e1526760381233.png)

Remember how many electrons are in one coulomb? If we had a Current that was 1 Coulomb per Second (1C/s). That would equal 6.241509 x 10^18 Electrons / Second (I just replace 1 Coulomb with the amount of electrons it is equal to), which is a lot of charge! 2 Coulombs per second would double that electron amount, which means more charge is flowing.

Current is measured in Amperes, so we can replace current in the unit equation with:



1 Ampere = 1 Coulomb / Second



This also means that Amperes is proportional to Coulombs, as long as that 1 second stays consistent. To translate, that means that 2 Amperes = 2 Coulombs / Second, or 4 Amperes = 4 Coulombs / Second.



Question time! If we have 20 Amperes of current, how many Coulombs would that be in a second? How many electrons would that be?



Amperes is always equal to Coulomb / Second, so if we have 20 Amperes, we have 20 Coulombs / Second.



1 Coulomb is equivalent to 6.241509 x 10^18 electrons, so _20_ Coulombs would be equal to:

![coulomb-conversion](https://thalliatree.files.wordpress.com/2018/05/coulomb-conversion.png)

20 Coulombs x 6.241509 x 10^18 electrons ≅ 1.2483018 x 10^20 electrons!

Let’s make sure we remember what we've covered so far.

Charge is a property of matter, and it's measured in Coulombs. 1 Coulomb is equal to a _lot_ of electrons.

Current is the amount of charge moving past a certain point each second, measured in Amperes.

Now, to understand what Voltage really is, we need to examine more about the side effects of charge.

Each individual proton and electron produces an **electric field**. These **electric fields** correspond to it's charge, so if it is a **proton (or a positively charged particle)**, the electric field is _positive__._ If it is an **electron (or a negatively charged particle)**, the electric field is _negative__._

![electron repel](https://thalliatree.files.wordpress.com/2018/05/electron-repel-e1526708116676.png)

See how these electron's electric fields surround them, and repel each other? That mass of blue in the middle shows they can't grow any closer. The force lines (with arrows) show where the electrons will go if the two are pushed any closer.

Same with these protons:

![proton repel](https://thalliatree.files.wordpress.com/2018/05/proton-repel-e1526708208199.png)

Like charges repel. Because of the electric fields around them, they cannot grow any closer. Therefore, their force is pointed away in the other direction.

But if you have a proton and an electron:

![electron proton attract](https://thalliatree.files.wordpress.com/2018/05/electron-proton-attract1-e1526709184274.png)

Opposites attract...do you see how the forces of the electric fields are drawn towards each other? They will get closer and closer until the +1.60217662 x 10^-19 and -1.60217662 x 10^-19 charges cancel each other out.

**An electric field is like a bubble that exerts a force on other charges to attract or repel them.**

If we had a stationary charge, a proton, like this one:

![stationary proton](https://thalliatree.files.wordpress.com/2018/05/stationary-proton-e1526709212953.png)

And we added another proton into the mix, what would happen to it, if this one (above) stayed stationary?

![proton-repel](https://thalliatree.files.wordpress.com/2018/05/proton-repel-e1526755401944.png)

The added proton, wherever placed, would be repelled in the direction the arrows were pointing, because the protons repel each other.

See how the added proton is forced towards the stationary one, but is repelled because the electric fields can only get so close?

[caption id="attachment_1846" align="alignnone" width="1200"]![proton-repel](https://thalliatree.files.wordpress.com/2018/05/proton-repel1.gif) (pardon my GIF making...it's the first one I ever tried!)[/caption]

If we kept the original proton stationary and added an electron, they would accelerate towards each other due to the attractive force of their fields.

These electric fields play a huge part in explaining what Voltage is.

Electric fields, as I said earlier, exert a **force** on other charged particles. This force is measured in Newtons. These forces can react in three different ways: repelling forces that can be overcome (a proton moving over a repelling group of like charges), attractive forces will come together (proton + electron), and repelling forces that are too strong are pushed away (simply repelling).

The force needed to overcome moving past a group of repelling charges is called **instantaneous electric potential.** Instantaneous just implies the electric potential at one super-specific point in time. Instantaneous electric potential has a unit of:

![joule-per-coulomb](https://thalliatree.files.wordpress.com/2018/05/joule-per-coulomb-e1526760685291.png)

If you remember in chemistry and physics, A Newton*meter is the unit for a **Joule**. This means we can simplify Nm/C to simply be J/C. But what does that actually represent?

![joule-per-coulomb](https://thalliatree.files.wordpress.com/2018/05/joule-per-coulomb1-e1526760713766.png)

A Joule is representative of _energy_. Energy is, by definition, **the ability to do work**. Work, scientifically defined, is exerting a force on an object to move it over a distance. A Joule, then,  is a measurement of the amount of energy it takes to accomplish exerting a variable force on an object to move it over a distance.

![work-per-coulomb](https://thalliatree.files.wordpress.com/2018/05/work-per-coulomb.gif)

Taking this definition, we can define what instantaneous electric potential is ourselves.

Instantaneous electric potential is the amount of Joules--the energy to move charges over a distance--per Coulomb of charge--that huge number of electrons--over a distance (this distance is often the opposite force exerted by the electric fields the new charge must overcome).

So, if we have 1 Joule of energy (the ability to exert a force on the charges to move them), and with it we move 1 Coulomb of charge (6.241509 x 10^18 electrons!), we have the equivalent of 1 Volt.

Keep in mind, this is only instantaneous electric potential (instantaneous Voltage), and charges are moving all the time! So this isn't wholly useful, yet.

Voltage is accurately described as electric potential **_difference_**, and this is only slightly different from instantaneous voltage.

If we have a group of charges:

![group-o-electron](https://thalliatree.files.wordpress.com/2018/05/group-o-electron-e1526761876388.png)

And little Mr. Electron wants to get past them:

![group-o-electron2](https://thalliatree.files.wordpress.com/2018/05/group-o-electron2-e1526762013104.png)

The force he needs to get around these guys at the beginning point (lets call it point A):

![group-o-electron3](https://thalliatree.files.wordpress.com/2018/05/group-o-electron3-e1526762131585.png)

Is much larger than the force he needs to move once he's gotten past them:

![group-o-electron4](https://thalliatree.files.wordpress.com/2018/05/group-o-electron4-e1526762492890.png)

The electric potential difference of this situation is the **Final** electric potential (at Point B) _minus_ the **initial** electric potential (at point A).

If you remember from algebra, subtracting the initial value from the final value gives us the **difference**. And this, my friends, is what Voltage is: electric potential difference. The difference between the **initial amount** of force per charge to move it over a distance, subtracted from the **final amount** of force per charge to move it over a distance.

Take this 3V battery. The top part of the battery labeled (+) is a positively charged plate of metal. The bottom part of the battery labeled (-) is a negatively charged plate of metal. When you connect the two, the opposite charges are attracted to each other, and the charged particles move because of that attractive force.

![battery-circuit](https://thalliatree.files.wordpress.com/2018/05/battery-circuit-e1526762713587.png)

Because this battery is 3 Volts, that is the amount of Joules per Coulomb needed to move the protons and electrons through this circuit. (3 J / 1 C = 3V) There are no obstructions for the charges to overcome, so the current (charge per second!) is constant.

Another real world example can be had with a common circuit component: Capacitors!

Capacitors are cool little nuggets. To put it simply, capacitors store charge. They have two metal plates on the inside, and the plates are separated by an insulator (material that does not conduct charge).

One of the leads is connected to the electron flow, and the other is connected to the proton flow. The metal plates begin to collect charge, but the charge can never meet in the middle due to the insulator. Therefore, a ton of positive and negative charge is built up on each plate, creating one massive electric field equal to the net charge of each individual electric field.

![capacitor diagram](https://thalliatree.files.wordpress.com/2018/05/capacitor-diagram-e1526762906558.png)

This electric field becomes an obstruction for charge to go past, which adds to the difference in electric potential magnitude. More Joules are needed to move the same number of Coulombs past the big electric field, which means a higher voltage is needed.

![capacitor diagram2](https://thalliatree.files.wordpress.com/2018/05/capacitor-diagram2-e1526763002585.png)

Voltage is an extremely confusing subject if you don't have a good understanding of the foundational principles of charge, energy, and work. But with a little thinking, it's an incredible subject to be able to understand.

{thallia}

P.S. There will be a follow up post sometime soon with a visual on the calculus definition of voltage.

* disclaimer: there _is_ a difference between chemical and physical properties, but the idea of a object or particle sporting a property is still the same.
