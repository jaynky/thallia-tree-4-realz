---
layout: post
author: thalliatree
comments: true
date: 2018-06-05 00:26:08+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/06/05/schematics-spinal-terror/
slug: schematics-spinal-terror
title: "Schematics : spinal-terror"
wordpress_id: 1866
post_format:
- Quote
---

If you don't understand a ton of circuit theory, have experience reading schematics, or played with a lot of circuits, then building your own schematic can seem daunting.

It was definitely daunting for me, the first time I built one. Getting used to EagleCAD, MCUs, and all the ins and outs of voltage and ground threw me for a spin a couple of times.

But once you get a few done, schematics aren't as hard as you think. As long as you know what pins are connected to Voltage and what pins are connected to Ground, you're all good to go.

Here are a few things I've learned when drawing up schematics:



	
  * Label all the pins. You'll be so glad you did.

	
  * Pull all the pins to voltage and Ground first, so those are out of the way.

	
  * Schematics are meant to be pretty and easy to read, so crisscrossing a ton of wires isn't very pretty...nor is it helpful in the process of reading it.

	
  * Different MCUs (microcontroller units) have different design guidelines to function. If you read the datasheet carefully, there are some rules you have to follow to get the MCU working, such as:

	
    * Capacitors on the outputs of pins

	
    * Resistors on the Reset pin

	
    * Clocks or oscillators on the XTAL pins

	
    * And much more...so take care to read through other people's usages of microcontroller chips and what they need to work by themselves before setting up the design of your own project.




	
  * Make sure none of the chips need a step down voltage converter (e.g. if your power source is 5V, but the chip can only handle 3.3V)


These are all guidelines I make myself think through as I'm designing the first-blood schematic of a project - most of them save you hours of agony later on when you can't figure out why your circuit isn't working.

Speaking of first-blood schematics, this is the brand new schematic for Spinal-Terror, a posture sensor that fouric, gector, and I are working on:

![screenshot.png](https://thalliatree.files.wordpress.com/2018/06/screenshot.png)

It could stand to be a little prettier...but it works for now ;)

I've been itching to work with my hands lately and Spark is taking a little bit of a break, so I'm going back to ArkenPi and giving it the upgrade it deserves. More on that later!

Happy hacking!

{thallia}
