---
layout: post
author: thalliatree
comments: true
date: 2018-06-08 00:08:52+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/06/08/arkenpi-the-revival/
slug: arkenpi-the-revival
title: "ArkenPi : the revival"
wordpress_id: 1870
post_format:
- Quote
---

With all these projects I've accomplished over the past year, I figure I need to start a new one over the summer so I'm not bored, or embellish the ones I've already finished. Fouric's project, spinal terror, is great but I'm waiting for him to get out of college classes so that we can work on the project more consistently. Starting a new project is pretty much out of the question because I am working this summer to save for college, so spending a lot of money on a new project isn't very reasonable. I came to the conclusion that I should improve on some of my finished projects! I'm going to start with ArkenPi, since my  binary watch is pretty fresh in my head. I need a break from that one.

ArkenPi worked great for a while but failed pretty fast.



	
  * The battery died super fast

	
  * The PiTFT screen protruded from the top of the case

	
  * The axes of the touchscreen were swapped

	
  * The USBs didn't connect all the way, some of them worked on some of them decided not to work whenever they wanted to.

	
  * And the keyboard...I just don't like the feeling of it.


All that to say, I am redoing the case and keyboard for ArkenPi!

I went and had ahead and stole the PCB from the keyboard I already had. I designed some new plastic caps, a frame, and a case for the keyboard, and I'm having Gector 3D print them for me.

[caption id="attachment_1872" align="alignnone" width="1600"]![screenshot (3)](https://thalliatree.files.wordpress.com/2018/06/screenshot-3.jpg) Keycaps & Rods[/caption]

[caption id="attachment_1871" align="alignnone" width="1600"]![screenshot (2)](https://thalliatree.files.wordpress.com/2018/06/screenshot-2.jpg) Case, Frame, & Tab[/caption]

All the ports used for USB, HDMI, audio, and the microSD card are all going to be in the top part of the case alongside the PiTFT. The battery and keyboard are going to be in the lower part of the case. I'm designing A new swivel-like hinge for the top and the bottom so the case will be able to open and close more smoothly.

![img_6215](https://thalliatree.files.wordpress.com/2018/06/img_6215.jpg)

It's a work in progress and takes a lot of time, but it's a ton of fun. I'm learning a lot of about fusion 360, and I get to use my new caliper tool! Check him out:

![img_6217](https://thalliatree.files.wordpress.com/2018/06/img_6217.jpg)

I also managed to figure out why the PiTFT's axes were swapped: when I dragged my finger right, the cursor went up. When I dragged my finger down, the cursor went to the left. It was a paaain.

But! As it turns out, any Raspberry Pi OS (Jessie and higher) use the touchscreen driver `libinput` , whereas the PiTFT only communicates with the old touchscreen driver, `evdev` . All I had to do was open `/usr/share/X11/xorg.conf.d/40-libinput.conf` and change the line with `Driver "libinput"` to `Driver "evdev"` for the `touchscreen` section.

Then I ran  `sudo apt-get install xserver-xorg-input-evdev`
Rebooted the Pi, and the touchscreen axes were working! Woohoo! There's one of the big problems fixed. :)

Happy hacking!

{thallia}
