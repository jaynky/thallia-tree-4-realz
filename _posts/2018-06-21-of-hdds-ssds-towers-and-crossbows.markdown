---
layout: post
author: thalliatree
comments: true
date: 2018-06-21 05:08:56+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/06/21/of-hdds-ssds-towers-and-crossbows/
slug: of-hdds-ssds-towers-and-crossbows
title: Of HDDs, SSDs, Towers, and Crossbows
wordpress_id: 1878
post_format:
- Quote
---

Sup nerds!

Been a while since I posted a thing. Graduation is nigh, as is my work schedule, so I haven't had a ton of time to work on projects.

At the most recent nerd hackout with UTW, one of the nerd's brothers gave me a tower computer! I also received old server hard drives from a fellow hacker as a grad gift. Totally awesome. :D

Today has been the day of testing all the SSDs and hard drives I've had lying around, which has been a little bit of a pain.

The HDDs I've had some experience with.

When plugging them into the computer with my SATA cable, I checked what disks were what with:`sudo fdisk -l`

Then `sudo fsck /dev/sdb1` . `fsck` returned some funky errors that I had some trouble interpreting, like `ext2fs_open2: Bad magic number in super-block`. I think since those HDDs/SSDs didn't have filesystems it couldn't find the filesystem itself, so it threw an error. I could also be wrong, but that's my hypothesis for now.

I ended up using `badblocks` to check the memory integrity of the chips, and that worked very well.

`sudo badblocks -v -s /dev/sdb`

If that ran into errors, it asked whether or not I wanted to fix the block of memory, in which you can choose yes or no. At the very end, it returned:

```
/dev/sdb1: ***** FILE SYSTEM WAS MODIFIED *****
/dev/sdb1: 126839/9519104 files (1.1% non-contiguous), 6480648/38072576 blocks
```

And running fsck on them again returned:

```
fsck from util-linux 2.30.1 e2fsck 1.43.5 (04-Aug-2017)
/dev/sdb1: clean, 126839/9519104 files, 6480648/38072576 blocks
```


So that's good :D

I've gotten 2/3 2.5" drives tested, and once those are ready I can get the tower computer set up. Once the tower is set up, I can go ahead and check the integrity of the server hard drives that Jay gave me as a grad gift.

It's fairly easy to check HDDs to see if they work, given that you read and research the error messages well.

I'm getting all the OSs ready to download onto the tower computer, I'll get a post up about that soon.

Until then!

{thallia}

P.S. I bought a crossbow too, it should be coming in the next few days. So excited!!
