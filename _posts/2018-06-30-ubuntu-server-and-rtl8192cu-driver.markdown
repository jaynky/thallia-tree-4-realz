---
layout: post
author: thalliatree
comments: true
date: 2018-06-30 02:46:45+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/06/30/ubuntu-server-and-rtl8192cu-driver/
slug: ubuntu-server-and-rtl8192cu-driver
title: Ubuntu Server and RTL8192CU driver
wordpress_id: 1881
post_format:
- Quote
---

Sup hackers!

I finally got around to setting my server back up again with new hard drives, and this time a new operating system as well.

Jay gave me 2 server hard drives as a graduation present, as mentioned in my previous post, so I tested those and installed the one that was working into my server. Here are some fun pics:

![img_6425](https://thalliatree.files.wordpress.com/2018/06/img_6425.jpg)

![img_6412](https://thalliatree.files.wordpress.com/2018/06/img_6412.jpg)

![img_6423](https://thalliatree.files.wordpress.com/2018/06/img_6423.jpg)

I attempted to load Ubuntu server onto the server with a USB, but I kept getting the error `linuxiso.bin is corrupt or missing`.

Uh oh.

After getting nowhere researching about that, I ended up burning the Ubuntu Linux Server ISO to a DVD and installing from there, which worked splendidly. I plugged the server in and that familiar purple screen popped up asking if I wanted to install the server.

That started the process of the download, just following the instructions on what you want the hostname to be, what hard drive you want the OS to occupy, etc. I did run into errors where the OS wouldn't install, but a quick plug into the router with an ethernet cord fixed that right up. Yay!

After that, I had the joy of figuring out the Edimax 7811Un wifi adapter again. [Remember the pain](https://thalliatree.wordpress.com/2017/12/05/edimax-ew-7811un-wifi-adapter-on-freebsd-server/) from last time? Yeah, this was a completely different setup so I had to throw all that experience out the window.

I found some documentation and stack overflow posts that hinted at a git repository you had to download due to some errors in the Ubuntu drivers:

https://askubuntu.com/questions/612649/edimax-ew-7811un-wifi-adapter-not-working-on-ubuntu-14-04-lts

https://askubuntu.com/questions/509498/is-there-a-standard-wifi-driver-for-the-edimax-ew-7811un#612662

I initially followed the latter stack exchange post, but ran into some issues.

`sudo apt install linux-headers-generic build-essential dkms git`

`git clone https://github.com/pvaret/rtl8192cu-fixes.git`

So far so good...

`sudo dkms add ./rtl8192cu-fixes`

and finally, my first error came from this command:

`sudo dkms install 8192cu/1.10`

It said something along the lines of `1.10` being nonexistent. A quick `cd` into that directory let me know that there was an updated version, `1.11`, so I used that.

`sudo depmod -a`

and then you were supposed to blacklist the native kernel driver with this command:

`sudo cp ./rtl8192cu-fixes/blacklist-native-rtl8192.conf /etc/modprobe.d/`

But after rebooting, nothing happened. It wouldn't recognize the driver at all. After researching more and rebooting about 10 times, I still couldn't figure it out. I followed all the instructions correctly.

And it was time for bed, so I gave it a rest.

Today, I found some sort of helpful posts, but none of them addressed why the wifi adapter wasn't being recognized. So I put on my debugging hat and tried to poke at the issue from a different angle to see if I could get any information that way.

I poked around with `lsmod | grep rtl8192cu` , but that returned nothing, so I tried to activate the driver with `sudo modprobe rtl8192cu` , but still that did nothing.

It was literally by a chance of mistype that I discovered the errors of my ways.

`lsmod | grep 8192cu` returned a line that said the driver was active!

So getting rid of the `rtl` at the beginning was apparently the right thing to do.

That means I had the correct driver, but perhaps the older one or the kernel was overwriting it. That means I had the blacklisting done wrong, so I `cd`'d into `/etc/modprobe.d/` and deleted the `blacklist-native-rtl8192cu.conf` file.

I noticed there was a singular `blacklist.conf` file in there, so I `sudo nano`ed into that and discovered a whole list of blacklisted/ignored devices. Perfect!

Scrolling to the bottom, I added,

`blacklist rtl8192cu` with a comment above it telling _why_ I was blacklisting it in case I ever needed to reference that again. After saving and quitting, I rebooted the server.

Lo and behold, the little blue blinking light on the adapter was resurrected from the dead. WOO!

But....now I had to actually get it connected to the internet. Because this was a brand new install I didn't have `nmcli` on there yet, so:

`sudo apt install network-manager`

Then to scan the _name_ of the edimax adapter, I did:

`nmcli dev`

Which showed me a short list of all the ports my server can use to connect to wifi. There were two ethernet ports, a loopback, and viola, a wifi!

It was a long device name though: `wlx74da38bd6202`

That was fun to type over and over.

To check the status of the adapter, I followed this series of commands:

`iw wlx74da38bd6202 link` :: which returned `no connection.`
`nmcli dev wifi list` :: which returned a list of the SSIDs.

To connect to the network:

`nmcli dev wifi connect SSIDNAME password ACTUALPASSWORD`

and with that, you should be able to `ping 8.8.8.8` to see whether or not you successfully connected!

I pinged 8.8.8.8, but I got the error saying `Reply From < IP address >: Destination Host Unreachable`

Oh no. Was something broken?
After some digging around through stack overflow again, my life was saved by a comment in [this](https://stackoverflow.com/questions/22110622/ping-response-request-timed-out-vs-destination-host-unreachable#22143974) post:


<blockquote>in case anyone else lands here I figured it out: My Linux server was configured with an Ethernet interface with static IP (which I pulled the cable out of once wifi was working) and a Wireless interface that was actually connected. Because of the static IP the Linux server saw the Ethernet interface as still enabled even though it didn't have a cable any more, and (I think) was trying to reply to my Wireless ping on the Ethernet interface... or something like that. Disabling the Ethernet interface fixed it, anyway! ~ Matt</blockquote>


Bless you, Matt.

I went through the names of the ethernet ports I saw in `nmcli dev`, and disabled them one by one with `sudo ip link set DEVICENAME down` .

After a `ping 8.8.8.8` once more, packets were successfully being transferred back and forth! Yayy!!

Now I can set up my SSH ports and webserver and possibly a game server for hacking this one special game if you guys are lucky...

{thallia}

P.S. I have a discord server where we discuss and solve problems like this! Come join us and create a digital hackerspace community :) [Join here!](https://discord.gg/sCkCkyw)
