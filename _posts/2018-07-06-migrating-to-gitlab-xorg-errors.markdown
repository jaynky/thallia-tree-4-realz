---
layout: post
author: thalliatree
comments: true
date: 2018-07-06 00:49:05+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/07/06/migrating-to-gitlab-xorg-errors/
slug: migrating-to-gitlab-xorg-errors
title: Migrating to Gitlab & xorg errors
wordpress_id: 1888
post_format:
- Quote
---

With the recent news of Microsoft purchasing GitHub, I've increasingly been wanting to try to port all of my repositories over to GitLab. And believe me, it's super easy and totally worth it!

Once you've signed up for GitLab, you can click the "add project" button in the top right by the search bar. It'll pull up a page where you can enter normal repo information, but there are also tabs that show different options:

![screenshot](https://thalliatree.files.wordpress.com/2018/07/screenshot.png)

The Import Project tab is what you want!
Gitlab'll give you some options from there:![screenshot.png](https://thalliatree.files.wordpress.com/2018/07/screenshot1.png)

And you can choose where you'd want to import from.

I imported all my GitHub repositories, and that was great...until I realized all of the cloned repos on my laptop are still connected to GitHub. So how does that work? It's not too hard.

Just find the repository you want pushing to Gitlab instead, and run these commands:

`git remote remove origin`
`git remote add origin https://gitlab.com/username/projectname.git`
`git fetch --all`

And boom! Everything is all ready to go :)
(sourced from [here](https://stackoverflow.com/questions/20359936/import-an-existing-git-project-into-gitlab))


* * *



On another note, I've been getting odd errors in my terminal when I execute programs that require a display, like Rofi, pcmanfm, and more.

`Failed to open display: (null)`
`Cannot open Display`
And finally, even xorg errored,
`xorg: giving up`

After some hunting, it seems like all I had to do was reinstall xorg.

`sudo apt-get install --reinstall xorg`

Then a reboot fixed those errors right up :)

{thallia}
