---
layout: post
author: thalliatree
comments: true
date: 2018-07-27 04:11:23+00:00
# A uselss commentlink: https://thalliatree.wordpress.com/2018/07/27/3d-printing-octoprint-server-updated-workstation/
slug: 3d-printing-octoprint-server-updated-workstation
title: 3D printing/OctoPrint Server/Updated Workstation
wordpress_id: 1896
post_format:
- Quote
---

Guys, only one month before I move into college. One month to do _all the things._

I've already gotten started by buying myself a 3D printer, a Monoprice Maker Select v2!

![img_6637.jpg](https://thalliatree.files.wordpress.com/2018/07/img_6637-e1532664478739.jpg)

I bought it from a guy who had gotten a brand new 3D printer for himself, and apparently had spent around $400 on upgrades and mods for the one he was selling. I got it for a steal at only $200.

![img_6650](https://thalliatree.files.wordpress.com/2018/07/img_6650.jpg)

Every time you physically move the printer or after a good lot of prints, you have to level the printer bed so the nozzle has an accurate "flat" surface to print on. It's a bit difficult at first, and super frustrating at times. But once you understand which ways the knobs turn, it gets a lot easier.

So far I've been messing up a lot of prints learning how the bed leveling works, practicing with smaller and shorter projects to get used to how it functions.

My first successful print (aside from test prints) was a silver coin stand :D Super cute, isn't it?

![img_6648](https://thalliatree.files.wordpress.com/2018/07/img_6648-e1532662683542.jpg)

Another thing I had to do to actually fit my 3D printer into my room (where my workstation is) was to rearrange some furniture to make room for a new worktable.

![img_6662](https://thalliatree.files.wordpress.com/2018/07/img_6662-e1532664639986.jpg)

And now everything is sturdy and awesome, aside from the fact that nothing is organized.

Here he is in action!

[First Print on Monoprice Maker Select](https://youtu.be/Bj2zcXhaIvc)

I think I'll name him Bernie. He just seems like a Bernie.

To slice my STL files, I use[ Cura](https://ultimaker.com/en/products/ultimaker-cura-software), a free software that's super easy to use. You launch the software, import a file, then adjust the direction in which you want your design to print. Enter the infill and precision, move parts around and print 2 at once, then slice it into a gcode file that you load onto the microSD card :)

I also set up a temporary OctoPrint server on one of my free Raspberry Pis. I burned the [OctoPi image](https://octoprint.org/download/) onto and SD card, then edited the "wpa_supplicant" file on it to my wifi SSID and password. To make your server secure when it's set up, I highly recommend SSHing into your Pi once it boots and changing the root password with `passwd`. Plug the Pi into the microUSB port onto the printer, and don't forget to plug it into power too!

Once that's done, you can go to http://octoprint.local/ or http://your-pi's-ip-address/ and you can access the Octoprint page, where you can upload files and print things over the internet :D Super cool.

I haven't toyed around with all the features, but it seems like it can be really useful if you read more into the documentation.

happy printing!

{thallia}
