---
layout: post
author: thalliatree
comments: true
title: thallia's tree Discord server
description: Come join the digital makerspace community!

---

Community. It's a great thing to have sometimes, especially in the programming/hacking/coding area of the net.

So without further adieu, I present to you the thallia tree discord server!

It's intended to be a place of interaction, learning, problem solving, and resource sharing.

[thallia tree's discord server](https://discord.gg/sCkCkyw)

There are multiple channels for multiple subjects, and now the only thing I'm missing is members. :)

![screenshot.png](https://live.staticflickr.com/65535/48515277452_94c7ebbfcd.jpg)

Everyone is welcome to come and ask questions in the respective channels, share current projects in `#show-and-tell`, and help contribute to building a community of nerds.

hope to see you all soon!

{thallia}
