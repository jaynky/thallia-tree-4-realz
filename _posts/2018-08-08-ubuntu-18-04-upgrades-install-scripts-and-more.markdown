---
layout: post
author: thalliatree
comments: true
title: "Ubuntu 18.04 : Upgrades, install scripts, and more"
description: I was a few upgrades behind the most current Ubuntu release...and updating was a little difficult. Here's how I did it.

---

Today I ran `lsb_release -a` on my laptop and tower, and to my astonishment I was very behind on the upgrades.

My laptop was running Ubuntu 17.10, and my tower was running 16.04. Jeez.

Since 17.10 was the newest Ubuntu using GNOME instead of Unity, I was able to upgrade from the command line to the 18.04 version. Before that, I backed up both of my computers to be safe.

I ran all these:

```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade
sudo apt autoremove
sudo do-release-upgrade
```

Ubuntu will ask you a few questions and then download everything. After a reboot, my laptop worked like a charm!

My tower was another story, however. I ran that same list of commands, but it didn't recognize that 18.04 or 17.10 were available to upgrade to, so I ended up just doing a clean install of Ubuntu 18.04, then running Gector's cool new [install script for new systems](https://gitlab.com/CaptainGector/configs/blob/master/new_install.sh). I plan on writing a script similar to it, with my configurations and most-used software of course.

Once I clean installed, I had to use `xrandr` to let the computer know that my monitors are stacked on top of each other. As I tried `xrandr --output DVI-D-1 --above HDMI-1` my entire screen froze, and the bottom monitor shut off.

Super weird.

I  realized that my GTX 1050TI drivers weren't installed on this clean install, so I figured that was the next place to go.

Running `ubuntu-drivers devices` listed my graphics card and the recommended driver that went with it. Then I executed `sudo ubuntu-drivers autoinstall` and let it install all the drivers I needed.

After a reboot, I went to `arandr` this time and created my screen configuration, and boom! It worked :D

The next steps are to go through the main configurations I have for tmux, vim, i3, polybar, and fish, then make copies of those configurations and edit them for my tower computer. You can see my collection of configurations here: [https://gitlab.com/thallia/dotconfig](https://gitlab.com/thallia/dotconfig)

After figuring out all the configurations, I wrote a bash `new_install.sh` script, which you can find in my dotconfig repository linked above. This installs all the general programs I use and configures the new system to my keybindings, vim config, and tmux keybindings so everything can be used like normal. That way every time I have a new system, I don't have to spend an hour re-configuring it to my preferences.

To test the script, I installed [VirtualBox](https://www.virtualbox.org/), a virtual machine software for linux, Mac, and windows.

Once installing the `.deb` package with `sudo dpkg -i /path/to/virtualbox/file.deb`, I launched the software and answered the basic questions for making a virtual machine. Then it asked for an optical drive, in which I had no clue what to give it.

Apparently an optical drive is either a bootable USB with the operating system on it, or a `.iso` image file with the operating system. So I went and downloaded Ubuntu 18.04 Bionic Beaver.

When I tried loading the vm again, it gave me this error:


    this kernel requires an x86-64 CPU, but only detects an i686 CPU, unable to boot

That made me grumpy.
But I sucked it up and went to stackoverflow and found that if you set your virtual machine to 2GB of disk space (which I did), you can't support the 64-bit versions. So clearly the way to go was download the Ubuntu 16.04 (32-bit) version of linux. If that doesn't work, might as well try to change the settings.

While waiting for the ubuntu torrent to download, I searched around and attempted this solution, but stack overflow said if that one still doesn't work then my hardware doesn't support it. *tears*

Wow. So I tried the new 32-bit version, and a critical system failure occured. Great. Then I remembered my mac laptop: which happens to gloriously work with everything!

I set up virtualbox on linux there, all the same settings, loaded the .iso, and boom. Ubuntu 18.04 (64-bit) worked wonderfully. Aside from the fact that it seemed to freeze. Sometimes that can happen if you don't give the vm enough space, so I edited some of the storage settings and rebooted the virtual machine.

That fixed the freezing :) I went to my gitlab, downloaded my new install script, and ran it.

So many errors :(

But that's cool! I did pretty well for scripting the whole thing. I just got some package names wrong, so I went and looked them up, fixed them, pushed to gitlab, and tried again.

After debugging for a few, it finally worked! There are two things still left to debug, installing `rofi` manually and debugging why the script won't `cd` into a YCM directory.

Other than that, I'd say this project was successful.

Until next time! :D

{thallia}
