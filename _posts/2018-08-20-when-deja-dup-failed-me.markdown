---
layout: post
author: thalliatree
comments: true
title: When Deja-Dup Failed Me
description: Backing up computers is always a hassle...and it happened to me with Deja Dup. Here's what I did to fix it.

---

Since I started using Linux 2-ish years ago, I have messed up the partitions on my laptop multiple times trying to fiddle with stuff. Through that, I very quickly learned the lesson of computer backups.

Due to messing up the partitions on my disk (I think there was 12 random partitions), I've been wanting to to a clean wipe and reinstall *only* ubuntu as my main operating system, deleting all other partitions.

I rebooted my laptop yesterday, and realized I had moved my system configuration file folder, which ruined all the symlinks I had set up. I moved that folder back to where the symlinks were pointing to, but everything derped and started malfunctioning. I had just backed up my laptop a few days prior, so I figured this was the defining moment to wipe everything and restore from backup.

Good hecking heck.

I clean installed, wiped everything, got my backups all ready to go, launched deja-dup, and attempted to restore from backup. I kept getting error after error after error that it could not find the backup to restore from. This was because instead of backing up to the same folder destination every time, I had started a _new_ backup folder with the date I backed it up on. Deja-dup couldn't find multiple backups in a folder, because I had so many different backup folders. Apparently that's not how you're supposed to use deja-dup haha.

I consulted the trusty UTW systemadmin channel we have and, fortunately, uelen was still up, and with his help we figured out how to retrieve the files from the apparently un-readable backup.

Deja-dup stores its backups as incremental `.tar` files, so as long as we could unpack those we could get my files back. But doing them by hand was a pain....until we found this: [https://wiki.gnome.org/Apps/DejaDup/Help/Restore/WorstCase](https://wiki.gnome.org/Apps/DejaDup/Help/Restore/WorstCase)

Deja-dup uses a package called duplicity, which you can conveniently use on the command line as well!

I made a directory to restore to:


    mkdir -p /tmp/restore


Then ran this command:


    duplicity --gio --no-encryption file:///media/thallia/name-of-backup-drive /tmp/restore

and boom. After a few minutes of waiting, all my files were restored from backup to `/tmp/restore`. :)

I moved them all over to my current home directory, re-symlinked, installed some new programs, and now my laptop is working almost like a charm again!

There are a few things I still need to install and reconfigure, but I'm really happy I got this done and out of the way before I move into college this coming week.

Lesson reinforced: ALWAYS have a backup of your computer before doing *anything* to the hard disk.

{thallia}
