---
layout: post
author: thalliatree
comments: true
title: "Ham Radio gpredict"
description: A cool amateur radio tool that allows you to track the satellites.

---

One thing that's cool about the world of hamateur radio is that radio waves are _all around us._ You could be standing in the middle of a transmission going on, and not even know it!

There's so much that goes on behind the scenes, including the transmission and receiving from satellites around the world. To track these satellites, you can use a cool software called `gpredict`.

To install, download the `.zip` file from the [GitHub repository.](https://github.com/csete/gpredict)

Make sure you have the dependencies installed:

```
      sudo apt install libtool intltool autoconf automake libcurl4-openssl-dev
      sudo apt install pkg-config libglib2.0-dev libgtk-3-dev libgoocanvas-2.0-dev
```


then, in the main folder, run `./autoconfigure.sh`

Once that finishes, `sudo make` and `sudo make install` to finish the install.

What's super cool about gpredict is you can use it to remote control the RTL-SDR through gqrx, which I set up in [my last blog post](https://thalliatree.net/ham-radio-technicians-license.html).

To set up the remote control, you gotta go to `edit > preferences` in gpredict. In the `interfaces` tab, you'll want to add a new interface.

![gqrx setup](https://live.staticflickr.com/65535/48514974456_cc2d2c2987_b.jpg)

Next, we need to make sure gqrx is listening on the right port for gpredict's controlling.

In gqrx, go to `tools > remote control settings` and make sure you add your local IP address:

![gqrx ip](https://live.staticflickr.com/65535/48515152197_fc4dfaccde.jpg)

I put both...since I wasn't entirely sure at this moment in time (1AM after a long day of packing).

You'll also want to set up a home base in gpredict, so it can give you accurate time predictions for when the satellites will be in the atmosphere above you. You can set that up in `edit > preferences`, in the `ground stations` tab. From there, add a new one with your location, and it'll start predicting when the satellites will be in range.

Finally, you can connect gqrx and gpredict by 1) turning on the `remote control` in gqrx, and 2) finding the drop down menu in gpredict in the top right corner of the program and clicking on the `radio control` option. That will bring up a window like so:

![screenshot.png](https://live.staticflickr.com/65535/48515152202_f528fcc0c7.jpg)

In gpredict, there will be a little radar-like thing in the bottom left, and right above it it'll tell you the satellite that will be closest to you, along with a time increment. If you find that satellite in the `target` settings above, and click `Track`, gpredict will control gqrx to track that satellite's frequency, as well as adjust it based on the doppler effect.

In the settings (bottom right of the above little window), make sure to click `engage` so gpredict will connect to gqrx and start remote controlling it. As you can see, satellite AO-91 was passing over my house in 12 minutes at the time I took the screenshot.

Hearing the satellite AO-91 and SO-50 and catching some CW from it was awesome. It was especially fun to see and hear the satellite moving away by the tone that I heard, and the signals you can see. Pretty neat! Now I have to focus enough to actually start recording the CW that I hear, and start decoding it.

Until next time!

{thallia}
