---
layout: post
author: thallia
comments: true
title: Brain Imaging
description: For a school project I decided to learn and write about the basics of the medical world's ways of imaging the brain.
---

I have always been fascinated with the idea of electricity in the body, specifically neuron signals, whether it be the signals from the brain to the muscles, to the organs, or the communication between the different parts of the brain.

To think we could measure this electricity, or even see the brain as it's sending and receiving these signals is beyond incredible, and somewhere in my career I would like to deal with these technologies.

There are multiple ways of measuring and reading brain waves, all different in their approach. From the book, _Essentials of Neuroimaging for Clinical Practice_ by Darin D. Dougherty, Scott Rauch, and Jerrold Rosenbaum, I was able to learn a lot more about how these technologies work.

There are quite a few main ways used in medical practice today that all differ in approach to measuring and reading the brain. These consist of but are not limited to: CT/CAT (Computed Tomography/Computerized Axial Tomography) Scans, MRIs/fMRIs (Magnetic Resonance Imaging/functional MRI), PET (Positron Emission Tomography), and EEG/MEG (Electroencephalography and Magneticencephalography). That is a lot of big words, but by the end of this I hope to have educated myself (and you) well on how these all work.


## CT/CAT scans


Computed Tomography/Computerized Axial Tomography is a very prevalent technology. It is very widely available, and a quick way to produce a high quality image of the brain. It works similar to X-Rays, in the way that it sends gamma rays perpendicularly into the body and measures the resulting radioactive absorption of the body. CT scans work in just about the same way, except it takes the same picture multiple times, just rotated around the same plane. One side of the machine sends the gamma rays, and the other side receives the rays after they have passed through the body. Once the pictures are all taken, they can be compiled and overlap into one high resolution picture of the brain.


## MRI/fMRIs


Magnetic Resonance Imaging measures the magnetic susceptibility of the body. The MRI aligns the nuclei with strong magnet first. If the nucleus doesn't have balanced protons or neutrons, the atom has what's called "net spin", and "net angular momentum". When the atom has angular momentum and a magnet is applied, the atom responds with a signal, called a _precess_. If the atom is balanced, though, and has no net spin, then there is no response Precess means the vibration of an element, and with precess you have resonance. There are unfortunately a lot more mathematics I do not understand yet that come after this for the MRI machine to finish assembling the picture of the brain, so perhaps I'll save that for another blog post.

Functional Magnetic Resonance Imaging is slightly different than what was stated above. An MRI takes the picture of the brain and creates structural images, whereas the fMRIs calculate the differences in tissue, oxygen, and blood flow triggered by neural events with respect to space, time, and more. This allows for no radiation use, so you are able to use it much more frequently without having to be wary of the person's health.


## PET


Positron Emission Tomography is different from CT scans and MRIs. CT and MRI provide straight images of the brain, whereas PET/SPECT provides data on the brain based on radioactive decay. Unstable (radioactive) nuclides are introduced into the organism, and a sensor detects how fast and where the radioactive decay occurs, then organizes this reaction into an effective photo of the brain.

To explain this a little more scientifically, unstable elements or isotopes have an excess of protons. Because there are too many, they have to send out a proton, which after being sent out, collides with an electron. When they collide, apparently the mass of them both get turned into energy as gamma photons (light), and the PET camera can measure the gamma waves.


## EEG


Electroencephalography is a voltage measurement device, and is typically used in psychiatry practices to out-rule certain neurological disorders from patients. Electrodes (connected to an amplifier) are attached to the person's scalp, in very specific areas, and over time the change in voltage is measured.

Specifically, the electroencephalogram measures the change in [electric potential](https://thalliatree.net/spark-the-research-part-1.html) (voltage) from the excitatory postsynaptic potentials (EPSPs) and inhibitory postsynaptic potentials (IPSPs). E/IPSPs are the measure of how likely a neuron will fire and change voltage, which is a perfect place to measure the neurological activity in the brain.

Once this data is recorded, a mathematical operation called the Fourier transform must be performed on the signals to appropriately read the data.



* * *



All of these ways to measure the brain are incredibly complex and fascinating, and I really hope to work particularly with electroencephalograms in the future. I was surprised to learn how many of these used a high extent of radiation for measuring and compiling pictures, and it makes me want to research more how to reduce that or make it even safer than it is.

{thallia}

  * Disclaimer: if there is anything phrased wrong or conveys the wrong information, please send me an email at thallia@thalliatree.net and I'd be happy to edit my post.
