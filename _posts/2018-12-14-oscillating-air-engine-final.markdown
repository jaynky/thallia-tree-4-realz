---
layout: post
title: "Oscillating Air Engine: Final Product"
author: thallia
---

Last time I shared about the engine, I had a few parts completed. Now that the semester is over, my engine is complete, and I had a *wonderful* turnout. :)

I finished machining all my parts:
![exploded-view1](https://farm5.staticflickr.com/4883/46416194732_a9215fa31d_z.jpg)

And I played around with assembling it, but almost all of my holes were slightly smaller than the parts I had machined, so I played around with sanding the parts down and widening some of the holes.

I had a hard time getting it running at first, since I had machined everything so perfectly it was too tight to run. I guess that's not the worst problem in the world.
I also learned that reaming plastic is tricky, since the plastic gets hot and expands as you're reaming it, therefore when it cools down it's still a different size than what you wanted.

Here's the first time my engine ran: 

<iframe width="560" height="315" src="https://www.youtube.com/embed/xUgv8f-zoLA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

It was good, but definitely not optimal, since I was going for the speed category.

We diagnosed that the first thing I needed was a base plate to keep the triangles from moving, apparently if they aren't secure they can twist while the engine is going and seize it up. So we used acrylic glue and secured it to a square of acrylic.

That definitely helped the engine start running more, but it still wasn't as fast as I thought I could get it. 
Before I could optimize any more, the engine stopped a few seconds after it started. The pistons were super hot, and we hypothesized it was due to my pistons being delrin: they heated up because the engine was going so fast, and expanded, seizing up against the acrylic.

To fix this, we stuck my pistons in the lathe and held a file up to them to take some of the material off, then polished them with the cylinder. Once done, my engine worked splendidly.

![air-engine-done](https://farm5.staticflickr.com/4875/46416194592_4bca043473_z.jpg)

Last, we added some friction tape stuff that made the cylinder move smoother on the valve plates. This was the final outcome speed of my engine:

<iframe width="560" height="315" src="https://www.youtube.com/embed/TYp-Tkonv3w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

In the competition, my engine *flew*. It ended up running around 2756RPM, with the school record being in the 2300s, and I won second place in the speed competition.

Overall, I'm super proud of my results, and it was a super fun project to work on for my first semester at college. :)

{thallia}
