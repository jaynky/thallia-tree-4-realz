---
layout: post
title: "Project Hype: Heartbeat Sensor"
author: "thallia"
---

Gector and I have been working on a new project together, a heartbeat sensor. It uses a cool infrared LED with a receiver to bounce light off of the pulse in your finger (or wrist), and then send the received signal to an op-amp, which amplifies it and blinks an LED to your heartbeat.

We want to take it to market hopefully, so I'm in project planning mode. We've nailed the circuit down, next step is to connect it to an ESP32/ESP8266, so you can send the heartbeat signal to someone else.

![heartbeat-circuit-schematic](https://farm8.staticflickr.com/7881/31527353827_3a64a1ebcb_z.jpg)

Our first schematic of the TCRT5000 shows the receiver part of the sensor on the left, and the infrared LED on the right. We hooked the sensor up to an oscilloscope to measure the voltage change as the TCRT5000 caught the IR reflection and outputted a voltage change.

Based on the changes in voltage, we played with resistor and capacitor values until it was a reliable looking heartbeat on the oscilloscope.

[Oscilloscope Heartbeat](https://youtu.be/ZWirWXVqlA4)

![tcrt5000-circuit](https://farm8.staticflickr.com/7840/44650301590_f1dd00532d_z.jpg)

Now that the oscilloscope was reading our pulses accurately, we stuck an LM358 op-amp in the circuit to amplify the signal, then attached an LED alongside that so the LED pulsed (ha) to our heartbeat.

![tcrt5000-lm358-op-amp-circuit](https://farm5.staticflickr.com/4806/45568693395_e46fb336e0_z.jpg)

I took the circuit home and rebuilt it to double check that it worked with my components, and it did! Woot! Although I will admit to it being finnicky at first, and that was due to the fact that I put a 470 ohm resistor somewhere where it should've been 470k. Once we cleared that up, it worked great. :) So there's your lesson for today--always double check your resistor values!

Next steps, like I said above, are now connecting this thing to an esp8266 and connecting it to another one, so they can communicate to each other.

Until next time!

{thallia}
