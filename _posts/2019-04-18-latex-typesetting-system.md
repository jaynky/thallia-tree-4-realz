---

title: LaTex
excerpt: LaTex is an awesome document creator, it's like a tool that lets you code your own documents. It's super useful for writing scientific reports, official documents (like papers or resumes), and more!
layout: post
author_profile: true
comments: true
toc: true

---

Hey all!

As I'm nearing the end of my freshman year at college, I've been playing around with a super neat tool called [LaTeX](https://www.latex-project.org/). LaTeX is a "high-quality typesetting system; it includes features designed for the production of technical and scientific documentation."

I've used it now to write a s   peech as well as some technical documents--and I *love* it. LaTeX compiles to really fancy PDF documents that look beyond professional. Orginally, I started out with [Overleaf](https://www.overleaf.com/), an online editor for LaTeX that compiles as you write the document. Overleaf explains the basic concept of LaTeX very well:

```
LaTeX (pronounced LAY-tek or LAH-tek) is a tool used to create 
professional-looking documents. It is based on the WYSIWYM 
(what you see is what you mean) idea, meaning you only have focus 
on the contents of your document and the computer will take care 
of the formatting. Instead of spacing out text on a page to control 
formatting, as with Microsoft Word or LibreOffice Writer, users can 
enter plain text and let LaTeX take care of the rest.
```

What's amazing about LaTeX is it has the ability to easily input things such as mathematical functions (just with a few lines of syntax, like so: `t=\frac{v^2}{2g\mu + \frac{7}{5}r^2{\alpha}}`), figures and tables (for data, there's even support for circuit schematics!), easy formatting (tables of contents, body structure, etc), multilingual support, and other fun add-ons for chess diagrams, chemistry diagrams, almost everything you could ever want from a document creator.

Now, from hearing some of that you may be completely overwhelmed like I was, but let me show you what the form of a basic document looks like.

```
\documentClass[11pt, letterpaper]{article}
\usepackage[utf8]{inputenc}
\title{LaTex Demonstration}
\author{thallia}
\date{April 23, 2019}

\begin{document}

\maketitle


\end{document}
```

And that's all you need to start a basic document! Not to mention that, to fit your needs, all of those are customizable. Inbetween the `\begin{document}` and `\end{document}` tags you write your content, filling it with more customizations as needed. Here's what that looks like on a PDF:

![LaTeX Demonstration: Basic Document](https://live.staticflickr.com/65535/40718415273_a37ec7c309_z.jpg)

All I've played with so far is structuring my papers with headers and sections, as well as mathematics. To play with basic structures, you have a simple format like this:

```
\section{This is My Main Header}

with some content

\subsection{This is a smaller section within my main header.}

oo, more content!

\subsubsection{And now an even smaller section under the subsection!}

test, test, is this on?

\subsection{The second smaller section within my main header.}

:)

```

Which looks like this:

![LaTeX Sections and Subsections](https://live.staticflickr.com/65535/46768741055_8c70dbedfe_z.jpg)

Isn't that neat? LaTeX provides such an easy and fun way to write papers. Overleaf has some *amazing* tutorials on how to use certain packages and features of LaTeX, and you can find the easiest tutorial here: [Learn LaTeX in 30 Minutes](https://www.overleaf.com/learn/latex/Learn_LaTeX_in_30_minutes). To the left are all the tutorials based on the different packages:

- [Mathematics](https://www.overleaf.com/learn/latex/Mathematical_expressions#Introduction)
- [Circuits](https://www.overleaf.com/learn/latex/CircuiTikz_package)
- [Languages](https://www.overleaf.com/learn/latex/International_language_support)
- [Section Formatting](https://www.overleaf.com/learn/latex/Sections_and_chapters)
- [Chemistry Formulae](https://www.overleaf.com/learn/latex/Chemistry_formulae)

And there's so much more.

Now you may be asking how you can even set something like this up to work (assuming you don't use [Overleaf](https://www.overleaf.com/) as your online live editor). I skipped installing from source on my machine and installed some plugins for [Atom](https://atom.io/), which is the current text editor I've been playing with lately.

I installed these plugins for LaTeX:

- [LaTeX Compile From Within Atom](https://github.com/thomasjo/atom-latex)
- [LaTeX Syntax Highlighting](https://github.com/area/language-latex)
- [LaTeX Autocomplete](https://github.com/Focus/latexer)

I added a setting in **atom-latex** to compile on save (which you can configure in the package settings), which is `Ctrl-s` for me, so it'll compile every time I save the document as I'm working on it--which provides a super easy (and interactive) document development environment, so I can complete my work while having fun at the same time.

I'm continuing to experiment with LaTeX as I learn more and want to document more, and it's honestly a super easy learning curve. If you're worried about time but wish to create some professional-looking documents, I'd *highly* recommend checking out LaTeX as a tool for your development and documentation tool belt.

Good luck trying it out, comment below if you have any questions or want to share what you love about LaTeX!

Until next time,

{thallia}
