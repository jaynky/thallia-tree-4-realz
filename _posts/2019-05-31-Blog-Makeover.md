---

title: New Look, Same Great Content
excerpt: Release notes from Jay, the redesigner of thallia's blog.
layout: post
author_profile: true
comments: true

---

Greetings people.
 
One day, thallia said "you should help me with SEO". I was like "cool this should be easy".
Famous last words. The jekyll theme she used turned the whole site into spaghetti code: 
The `<head>` tags were not always at the top, it was practically impossible to change anything, 
and what little I could change was needlessly complicated. Thus I made the decision to completely
rebuild everything from scratch. Here are a few changes I made:

* Replaced the archive with a search bar 

* changed font

* made the footer fixed

* added a table of content section at the top

* optimized for speed

* Removed Kittens

Now that the entire website is rebuilt, I can start
the thing I was originally tasked with doing, and 
start SEO.


`~Jay`

About Author:

I build websites. If you like what I did, discord me at
jay#3130. If you are old you can email me at 
jaynkystudio@gmx.com. 
