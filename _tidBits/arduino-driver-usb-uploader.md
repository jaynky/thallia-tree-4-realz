---

layout: post
author_profile: true
image: https://farm8.staticflickr.com/7816/32049788997_8c2d0bf07b_z.jpg
title: Arduino Serial Port Error
description: "I kept getting an error that my computer couldn't access the COMM serial port, here's how I fixed it."

---



When I was trying to upload code to my Arduino UNO for a class, my laptop started throwing a permissions error--which I *could* fix by adding sudo in front of the arduino command, but when you add sudo to it, it messes up all your files and sketch permissions.

I also tried adjusting the serial port permissions and adding my user to the `dialout` group, but that still threw the same error.

So I needed to solve this. When I ran the arduino software, I realized that as soon as the program started, it threw this error: `java.lang.NullPointerException thrown while loading gnu.io.RXTXCommDriver`. 

Doing a little research, I found [this](https://askubuntu.com/questions/1034173/arduino-serial-communication-uploader-not-working) post.

I didn't have all the java packages and drivers, so I had to install them.

```
sudo apt install openjdk-11-jdk
sudo apt install openjdk-8-jdk

sudo add-apt-repository ppa:webupd8team/java
sudo apt update
sudo apt install oracle-java8-set-default
```

After installing all that and restarting the Arduino software, we were good to go!
