---
layout: post
title: Arduino in Rofi - Building from Source
description: My arduino IDE was bugging out, and I didn't like executing it from the terminal, so I fixed it.
image: https://farm8.staticflickr.com/7900/47079897981_cb1d154353_m.jpg
---

![Arduino-software-screen](https://farm8.staticflickr.com/7900/47079897981_cb1d154353_m.jpg)

I've been needing to use my Arduino IDE for a class I'm in at the moment, but it was being a dork and having me change all these permissions for me to access a port (which I fixed...) but I also didn't like having to execute it from the terminal and experiencing library issues, since I hadn't set it up right in the first place.

So the first step was (after clearing out all the old arduino IDEs I'd messed with), I installed the linux64 package from the [Arduino](https://www.arduino.cc/en/main/software) website.

After it downloaded, I moved it to my home directory and ran:
`tar -xf arduino-1.8.8.tar.xz` 
to extract it, then removed the archive with:
`rm arduino-1.8.8.tar.xz`

At this point it ran, but I didn't have the executable. 
```
cd arduino-1.8.8
./install.sh
```
and everything should exist.

But, this didn't solve my problem of wanting to not execute from the folder.
So I looked into what are called `$PATH` variables in linux, and for my specific shell,(fish shell for the win!) I found I needed to permanently add the arduino directiory into my `config.fish` file.

So I went into `~/.config/fish/` and created `config.fish`, then inside that file added:
`set PATH ~/arduino-1.8.8/ $PATH`
which adds that directory's executables to the `$PATH` variable.

But that still doesn't solve executing from the terminal.

So I did something sneaky, and symlinked the arduino executable to where my Rofi applications manager executes programs from:

`sudo ln -s ~/arduino-1.8.8/arduino /usr/bin/arduino`

And boom, problem solved. :)

Until next time!

{thallia}