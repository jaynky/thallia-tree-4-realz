---
layout: post
title: Jekyll Collections
description: Setting up jekyll collections, fast and easy!
image: https://farm8.staticflickr.com/7861/46290379704_593fff71da_m.jpg
---

Jekyll is a great static website generator that powers this very blog, here's how to easily implement some of the more interesting features: collections.

The website theme I use, [minimal mistakes](https://mmistakes.github.io/minimal-mistakes/) is a theme designed specifically for collections, actually, but I'd never heard of those before.

Collections are for if you want to have a, well, smaller collection of things like recipes, or portfolio entries. I wanted something similar, for smaller things I learn that don't require a full blown blog post.

[This](https://blog.webjeda.com/jekyll-collections/) post has some good descriptions about collections, and a good basis on how to set them up.

In `config.yml`, at the bottom, you can put something like this:

```
collections:
    - themes
    - designs
```

Where `themes` and `designs` are the names of your collection(s). If you want each page in your collection to generate its own page (as I've done), then you need to add a little more data.

```
collections:
    tidbits:
        output: true
        permalink: /tidBits/:path/
```

That's what mine looks like, above.

Once that's done, you need to create a file in your root directory that's the name of the collection, such as `tidbits.md` or `themes.md`. 

You'll also want to create a folder like so: `_tidbits`, `_themes`. This folder is where you'll put the files for the collection.

In `tidbits.md`, this'll be where you configure the front matter of the file to hold the collections.

Mine looks like this:

```
layout: collection
collection: tidbits
entries_layout: grid
permalink: /tidBits/
author_profile: true
comments: true
```

This sets up the page with all the files in `_tidbits` and displays them in a grid format, which you can see [here](https://thalliatree.net/tidBits/).

And that should be it--build your website with `sudo jekyll build` and it should load up correctly. :)

{thallia}
