---

layout: post
title: Mermaid - a flowchart tool
description: "Super neat tool: A simple markdown-like language for generating diagrams and charts from text."
image: https://farm8.staticflickr.com/7894/47424963461_c6d155b485_m.jpg
---

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/145969540@N06/47424963461/in/dateposted-public/" title="mermaid"><img src="https://farm8.staticflickr.com/7894/47424963461_c6d155b485_c.jpg" width="800" height="502" alt="mermaid"></a><script async src="https://embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

The other night I was thinking about writing out pseudo-code for PDA, my [semester long project](https://thalliatree.net/arduino-and-ra8875/) that's coming due in the next few weeks.

At that point, I didn't really wanna get a notebook and write it all out, and I'd been meaning to try this tool Gector had told me about--mermaid.

Mermaid is a super simple markdown-like language that, when run, generates really cool looking flow charts, gantt diagrams, and more. So far I've only messed around with the flowcharts (as seen with my flowchart above).

The basic syntax goes like so:

```
graph TD   // where TD means top down, and this creates a top-down graph

A[First Node] ---> B(Second Node)  // A is the first node, and the brackets denote the text that will be inside the node

B ---> C(Third Node)  // This connects the B node to the third one

A ---> D(Fourth node, second branch!) // this makes a new branch!

```
As you can see, it's pretty easy to use and understand. 
In the [documentation](https://mermaidjs.github.io/flowchart.html) there are a lot more options and configurations, such as adding color, putting more words in different places, the shapes of the nodes, and more!

It's a pretty fancy tool, fun to use, and ultimately very useful once you get the hang of it.

happy flowcharting!

{thallia}
