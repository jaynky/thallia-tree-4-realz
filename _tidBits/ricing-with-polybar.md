---
layout: post
author_profile: true
image: "https://live.staticflickr.com/65535/48127423342_bea1d4a3e3_z.jpg"
title: Ricing with Polybar (& i3)
description: Here's how I added icons and updated my polybar config.
---

![polybar rice](https://live.staticflickr.com/65535/48127423342_bea1d4a3e3_z.jpg)

Welcome back to another tidBit!

A while ago I set up my laptop with [polybar](https://thalliatree.net/tint2-replacement-polybar/), but I decided I needed an upgrade.

I found a beautiful polybar setup on [reddit](https://www.reddit.com/r/unixporn/comments/c2gtgu/bspwm_line_awesome/?utm_source=share&utm_medium=web2x), so I copied it and sat down to get it working on my laptop.

The first issue was getting the icons to work. I kept getting errors like this:

`warn: Dropping unmatched character  (U+e028)`

 To fix this, I needed to find what icon pack the original author was using. Down in the polybar config, there was a line that said:

`font-0 = Inconsolata:size=10;1`
`font-1 = lineawesome:size=13;1`

Lineawesome is a similar icon package to fontawesome, so I knew that was the font package I needed to get. Finding this page on [lineawesome's website](https://icons8.com/line-awesome/cheatsheet/) I was able to download the ZIP file. Once extracted, the `.ttf` file inside is what I needed.

Once you've located the `.ttf` file, you need to move it to the font directory on your laptop.

`mv ~/Downloads/line-awesome/line-awesome.ttf ~/.local/share/fonts/`

Once that's done, you have to reset the font cache with:

`fc-cache -f -v`

Running `polybar <name of bar>` after that displayed the icons! Woo!

After that, it was smooth sailing for customization. Every button now launches the program I want:

* Search: Launches `gnome-terminal -e ranger` (a file explorer)
* Camera: Launches `flameshot gui -p ~/directory/to/save/screenshot` (flameshot is a great screenshot service)
* Compass: Launches `firefox` (browser)
* File folder: Launches `nautilus`
* Pen & Paper: Launches `atom`
* Music: Launches `rhythmbox`

You can find my polybar config here: [https://gitlab.com/thallia/dotconfig/blob/master/polybar/config](https://gitlab.com/thallia/dotconfig/blob/master/polybar/config)
