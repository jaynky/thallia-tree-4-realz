---
layout: about
title: about
description: About page for Thallia Tree
permalink: /about/
---

# About

Welcome to my tree. :)

I’m thallia, an undergraduate electrical engineer. I’m a maker, learner, and autodidact. I love learning. Learning how the world works always fascinates me, primarily the things we can’t see, like electricity and radio waves. I also love people, and want to always be involved in the community, and helping others to learn as well. 

My ultimate career goal is to develop new medical technology (diagnostic instrumentation) that can be used at home.

The reason I made this blog is:

* To provide documentation for my forgetful self

* To inspire others with my projects

* And to encourage the importance and fun of learning new things

I’m very passionate about the importance of learning, and I love the maker community for that very reason–encouraging learning through fun and nerdy projects. If you want to join an engaged and fun community, come on over to my Discord server! (link in footer)

What you'll see on this blog and my instagram will run along the lines of my hobbies, which include:
* Linux system administration
* Arduino, Raspberry Pi, any microcontroller
* Ham Radio
* Programming/Coding
* Reverse Engineering
* Ethical Hacking/Penetration Testing
* Debugging
* Building things


Thanks for stopping by!

fun fact (because everybody likes those): my pseudonym thallia originates from element 81 of the periodic table, thallium. I chose that elemental name as it comes after my mentor’s pseudonym, Mercury. (He also goes by the name fouric, which I’ll most likely reference him by on this blog.)

#  Comrades

Here are some of my fellow comrade's blogs:

* [Jay, freelance web developer](https://jstackstudios.com)
* [uelen, mathematics major and coder](https://finzdani.net/)
* [Gector, electrical engineer : TBD]()
* [Trevor, Embedded Software Engineer/Computer Engineer](https://gahl.tech/)

