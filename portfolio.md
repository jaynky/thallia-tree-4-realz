---
layout: portfolio
title: Thallia's portfolio
description: Thallia's excellent portfolio
permalink: /portfolio/
---

# Portfolio

I currently have my amateur radio technician's license (KJ7AGN), foundational skills in CNC machines (Mills and Lathes), 3+ years in soldering, foundational skills in CAD software such as EagleCAD, Blender, and Solidworks, and some experience with the Unity game engine. I have lots of experience with C++, and have played with C, assembly,  python scripting, C# scripting, and forth, giving me a strong programming fundamentals understanding. I have made many extensive projects with Arduino, as well as the standalone microcontroller from Atmel (ATmega328P). I hold lots of experience with the Linux/UNIX operating system, and have a strong knowledge of the command line. I am competent in Word and Excel, though my preferred typesetting system is LaTeX. Via my blog I have gained skills in technical writing and documentation.

At my university I am one of thirteen chosen this year to be a part of a rigorous international program called Grand Challenge Scholars, based on the Grand Challenge Engineering mission. I will be researching advanced health informatics with a computer science and biomedical professor.
        
