---
layout: tidBits
title: tidBits
description: tidbits of information
permalink: /tidBits/
---

A collection of tools, tricks, and small bugs that weren't worthy enough for a full blown blog post. Enjoy!
